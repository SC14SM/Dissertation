; Auto-generated. Do not edit!


(cl:in-package project-msg)


;//! \htmlinclude bounding_squares.msg.html

(cl:defclass <bounding_squares> (roslisp-msg-protocol:ros-message)
  ((count
    :reader count
    :initarg :count
    :type cl:fixnum
    :initform 0)
   (side_lengths
    :reader side_lengths
    :initarg :side_lengths
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (top_pixel_indexes
    :reader top_pixel_indexes
    :initarg :top_pixel_indexes
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (left_pixel_indexes
    :reader left_pixel_indexes
    :initarg :left_pixel_indexes
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0)))
)

(cl:defclass bounding_squares (<bounding_squares>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <bounding_squares>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'bounding_squares)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name project-msg:<bounding_squares> is deprecated: use project-msg:bounding_squares instead.")))

(cl:ensure-generic-function 'count-val :lambda-list '(m))
(cl:defmethod count-val ((m <bounding_squares>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader project-msg:count-val is deprecated.  Use project-msg:count instead.")
  (count m))

(cl:ensure-generic-function 'side_lengths-val :lambda-list '(m))
(cl:defmethod side_lengths-val ((m <bounding_squares>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader project-msg:side_lengths-val is deprecated.  Use project-msg:side_lengths instead.")
  (side_lengths m))

(cl:ensure-generic-function 'top_pixel_indexes-val :lambda-list '(m))
(cl:defmethod top_pixel_indexes-val ((m <bounding_squares>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader project-msg:top_pixel_indexes-val is deprecated.  Use project-msg:top_pixel_indexes instead.")
  (top_pixel_indexes m))

(cl:ensure-generic-function 'left_pixel_indexes-val :lambda-list '(m))
(cl:defmethod left_pixel_indexes-val ((m <bounding_squares>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader project-msg:left_pixel_indexes-val is deprecated.  Use project-msg:left_pixel_indexes instead.")
  (left_pixel_indexes m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <bounding_squares>) ostream)
  "Serializes a message object of type '<bounding_squares>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'count)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'side_lengths))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    ))
   (cl:slot-value msg 'side_lengths))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'top_pixel_indexes))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    ))
   (cl:slot-value msg 'top_pixel_indexes))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'left_pixel_indexes))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    ))
   (cl:slot-value msg 'left_pixel_indexes))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <bounding_squares>) istream)
  "Deserializes a message object of type '<bounding_squares>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'count)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'side_lengths) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'side_lengths)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536)))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'top_pixel_indexes) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'top_pixel_indexes)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536)))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'left_pixel_indexes) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'left_pixel_indexes)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536)))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<bounding_squares>)))
  "Returns string type for a message object of type '<bounding_squares>"
  "project/bounding_squares")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'bounding_squares)))
  "Returns string type for a message object of type 'bounding_squares"
  "project/bounding_squares")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<bounding_squares>)))
  "Returns md5sum for a message object of type '<bounding_squares>"
  "5479127de4796929bcf44917fbd80689")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'bounding_squares)))
  "Returns md5sum for a message object of type 'bounding_squares"
  "5479127de4796929bcf44917fbd80689")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<bounding_squares>)))
  "Returns full string definition for message of type '<bounding_squares>"
  (cl:format cl:nil "uint8 count					#The number of bounding squares in the message~%~%int16[] side_lengths		#The side lengths of each bounding square~%~%int16[] top_pixel_indexes 	#The index of the top edge of the square~%~%int16[] left_pixel_indexes 	#The index of the left edge of the square~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'bounding_squares)))
  "Returns full string definition for message of type 'bounding_squares"
  (cl:format cl:nil "uint8 count					#The number of bounding squares in the message~%~%int16[] side_lengths		#The side lengths of each bounding square~%~%int16[] top_pixel_indexes 	#The index of the top edge of the square~%~%int16[] left_pixel_indexes 	#The index of the left edge of the square~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <bounding_squares>))
  (cl:+ 0
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'side_lengths) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 2)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'top_pixel_indexes) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 2)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'left_pixel_indexes) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 2)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <bounding_squares>))
  "Converts a ROS message object to a list"
  (cl:list 'bounding_squares
    (cl:cons ':count (count msg))
    (cl:cons ':side_lengths (side_lengths msg))
    (cl:cons ':top_pixel_indexes (top_pixel_indexes msg))
    (cl:cons ':left_pixel_indexes (left_pixel_indexes msg))
))
