
(cl:in-package :asdf)

(defsystem "project-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "bounding_squares" :depends-on ("_package_bounding_squares"))
    (:file "_package_bounding_squares" :depends-on ("_package"))
  ))