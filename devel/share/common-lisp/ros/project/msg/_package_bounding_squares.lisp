(cl:in-package project-msg)
(cl:export '(COUNT-VAL
          COUNT
          SIDE_LENGTHS-VAL
          SIDE_LENGTHS
          TOP_PIXEL_INDEXES-VAL
          TOP_PIXEL_INDEXES
          LEFT_PIXEL_INDEXES-VAL
          LEFT_PIXEL_INDEXES
))