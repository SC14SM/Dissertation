#!/usr/bin/env python

#	Takes the existing depth image published by the kinect camera and converts it to unsigned 8 bit int format for use with openCV.
# 	Also, removes any pixels making up part of the floor from the image and downsamples it by a factor of 4 on both axes for performance increase, 
# 	since we don't need a very high resolution depth image.
#	
#	This node decides which pixels make up part of the floor by listening to the output of floor_mask_node.py

import rospy
import cv2
import numpy as np
import sys

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class depth_converter():
	
	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("Depth_Converter", anonymous = True)
		
		#Instantiate the cv bridge to convert the image for openCV
		self.bridge = CvBridge()
		
		#Create a publisher
		self.depthPublisher = rospy.Publisher("convertedDepth", Image, queue_size=5)
		
		#Create a subscriber to the depth image topic
		depthSubscriber = rospy.Subscriber("camera/depth/image_raw", Image, self.depthCallback)
		
		#Subscribe to the floorMask topic
		floorMaskSubscriber = rospy.Subscriber("floorMask", Image, self.floorMaskCallback)
		
		#Define an array to store the floorMask in - by default it is all 255 so that if no accurate floormask gets published then nothing is removed by the bitwise_and operation
		self.latestFloorMask = np.full((120, 160), fill_value = 255, dtype = np.int8)
		
		
	def floorMaskCallback(self, Image):
		#Upon receiving an update from the floormask topic, store it in a variable so it can be accessed by the other methods
		self.latestFloorMask = self.bridge.imgmsg_to_cv2(Image)
		self.convertAndPublishImage()
		
	def depthCallback(self, Image):
		#Upon receiving a new depth image, store it for use by another function
		self.latestDepthImage = Image
		
	def convertAndPublishImage(self):	
		#use the floor mask to eliminate pixels that form part of the floor
		Image = self.latestDepthImage
		
		try:
			#Convert the image into openCV format
			depthImage = self.bridge.imgmsg_to_cv2(Image)
			
		except Exception, e:
			print "unable to convert depth image to bgr"
			print e
			
		#Convert image from a 32bit floating point depth value to an unsigned 8bit integer array
		maxValue = np.nanmax(depthImage)
		imageScaled = depthImage / maxValue		#Scale to the range 0..1
		imageScaled = imageScaled * 255			#Scale to range 0..255
		imageU8 = imageScaled.astype(np.uint8)
		
		#Downsample the image for quicker processing time - we don't need an especially high resolution image for this stage anyway since we are just identifying general shapes
		smallImageU8 = cv2.resize(imageU8, (0,0), fx=0.25, fy=0.25, interpolation=cv2.INTER_NEAREST)
	
		#Remove pixels forming part of the floor
		smallImageU8 = cv2.bitwise_and(smallImageU8, smallImageU8, mask = self.latestFloorMask)
			
		#Publish the depth image
		self.depthPublisher.publish(self.bridge.cv2_to_imgmsg(smallImageU8))
		
		
def main(args):
	#if this is main path, launch an instance
	imageConverter = depth_converter()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	#Destroy all windows on exit
	cv2.destroyAllWindows() 
		
if __name__=="__main__":
	main(sys.argv)
