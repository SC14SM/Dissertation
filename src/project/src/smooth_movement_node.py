#!/usr/bin/env python

#	Node to translate a movement vector to base movement commands, uses variable speed to trace a smoother path than the stop_turn_go node
# 	The movement vector should be produced by a control node, which weights the various inputs to produce it - 
# 	this node is not concerned with the details of how the movement vector is produced, it simply returns a set of base movement commands to follow it.

import numpy as np
import rospy
import sys
import math 

from geometry_msgs.msg import Twist, Vector3
from std_msgs.msg import String

class smoothMovementNode():
	
	def __init__(self, namespaceIndex):
		
		#Initiate the ros node
		rospy.init_node("smooth_movement_node", anonymous = True)
		
		self.lastestVector = Vector3()
		self.vectorMagnitude = 0.0
		self.vectorBearing = 0.0
		
		#Subscribe to the vector topic
		vectorSubscriber = rospy.Subscriber("movement_vector", Vector3, self.vectorCallback)
		
		#Subscribe to the parameter topic
		parameterSubscriber = rospy.Subscriber("parameters", String, self.parameterAdjustCallback) 
		
		#Instantiate a publisher for movement commands
		self.movementPublisher = rospy.Publisher("movement_commands", Twist, queue_size=5)
		
		#Initialise the "linear cutoff" parameter, this defines the turning angle at which the turtlebot turns on the spot instead of moving and turning together
		self.linearCutOff = math.pi/4 	
	
		#Start the movement loop
		self.startMovementLoop()

	def vectorCallback(self, vector):
		#Called every time a new vector is published
		
		#Find the bearing and magnitude of the vector and store them
 		self.latestVector = vector 
 		self.vectorMagnitude = math.sqrt(math.pow(vector.y,2)+ math.pow(vector.x, 2))
 		
 		#Bearing is found by working out the dot product of the vector with a unit vector in direction x, over the dot product of the two vectors magnitudes
 		if not self.vectorMagnitude == 0:
			bearing = np.arccos(vector.x/self.vectorMagnitude)
		else:
			bearing = 0
			
		#Arccos will always give us a value in the range 0..pi which isn't always helpful, since we want to be able to distinguish between pi/2 clockwise
		# and pi/2 counterclockwise. We can solve by simply checking if the vector points to the left or the right of the turtlebot
		if vector.y < 0:
			bearing *= -1
		
		#Store the resulting bearing
		self.vectorBearing = bearing
	
	def parameterAdjustCallback(self, String):
		#Called when a message is received to adjust parameters
		
		#Start by splitting the string and taking only the entry that relates to this node
		splitString = message.data.split(",")
		self.linearCutoff = math.pi/float(splitString[2])
		
		print "Setting linear cutoff to %.2f" % (self.linearCutoff)
				
	def startMovementLoop(self):
		#Publishes movement commands continuously
		
		running = True
		
		#Define a timer, used to sleep between issuing movement commands
		tenHz = rospy.Rate(10)
		
		#Define some movement parameters
		maxTurnRate = math.pi		
		maxLinearSpeed = 0.4
		
		#Create a twist message for movement commands
		movementMessage = Twist()
		stationaryMessage = Twist()
		
		while running:
						
			#Calculate the turn speed and linear speed to use
			if abs(self.vectorBearing) > self.linearCutOff: 
				angularMovementRatio = 1.0
			else:
				angularMovementRatio = abs(self.vectorBearing)/self.linearCutOff
			
			if self.vectorBearing < 0:
				turnDirection = -1
			else:
				turnDirection = 1
				
			#Build and publish the twist message
			if self.vectorMagnitude == 0:
				self.movementPublisher.publish(stationaryMessage)
			else:
				angularSpeed = angularMovementRatio*maxLinearSpeed*turnDirection
				linearSpeed = (1-angularMovementRatio)*maxLinearSpeed
				movementMessage.linear.x = linearSpeed
				movementMessage.angular.z = angularSpeed
				
				self.movementPublisher.publish(movementMessage)
			
			#Sleep for 1 cycle after publishing the message
			tenHz.sleep()		
			
		
def main(args):
	movementNode = smoothMovementNode(args[1])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)
