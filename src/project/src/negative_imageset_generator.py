#!/usr/bin/env python

#	Very similar to imageset_generator, but produces images that do not contain targets.

import sys
import rospy
import math
import tf
import os
import time
import cv2
import random
import numpy as np

from cv_bridge import CvBridge, CvBridgeError
from gazebo_msgs.srv import GetWorldProperties, SetPhysicsProperties, SpawnModel, SpawnModelRequest, DeleteModel, SetModelState
from gazebo_msgs.msg import ModelState
from std_srvs.srv import Empty
from geometry_msgs.msg import *
from sensor_msgs.msg import PointCloud2, PointField, Image
from project.msg import bounding_squares
from tf.transformations import quaternion_from_euler

class ImageSetGenerator():
	
	#Define the list of items that will be included in the training set
	targetModelSet = ["../gazebo_objects/litter/cigarette_box/model-1_4.sdf", "../gazebo_objects/litter/cigarette_butt/model-1_4.sdf", "../gazebo_objects/litter/noodle_box/model-1_4.sdf", "../gazebo_objects/litter/styrofoam_box/model-1_4.sdf", "../gazebo_objects/litter/styrofoam_cup/model-1_4.sdf", "../gazebo_objects/litter/paper_ball/model-1_4.sdf","../gazebo_objects/litter/soda_bottle/model-1_4.sdf"]
	clutterModelSet = ["../gazebo_objects/environment_objects/cedar_bench/model-1_4.sdf", "../gazebo_objects/environment_objects/hardwood_bollard/model-1_4.sdf", "../gazebo_objects/environment_objects/planter/model-1_4.sdf", "../gazebo_objects/environment_objects/litter_bin/model-1_4.sdf"]
	wallModelPath = "../gazebo_objects/environment_objects/wall/model-1_4.sdf"
	
	#Some parameters to stop things spawning outside the FOV or inside eachother (causing weird collision bugs)
	modelMaxDistance = 0.8
	modelMinDistance = 1.5
	modelDistance = 0
	backgroundMinDistance = 2
	backgroundMaxDistance = 4
	backgroundMaxOffset = 0.25
	wallMinDistance = 1
	wallMaxDistance = 3
	
	#Keep track of how rotated the turtlebot is at any one point
	currentRotation = 0
	
	#Define how many images to generate
	imageSetSize = 1000

	#Define how much to downsample the image feed when choosing regions of interest
	downsamplingFactor = 0.25
	def __init__(self):
		#Start by initiating a ROS node
		rospy.init_node("Image_set_generator", anonymous=True)

		#Wait for services to be ready
		rospy.wait_for_service("gazebo/get_world_properties")
		
		#Create a cv bridge
		self.bridge = CvBridge()
		
		self.tenHzTimer = rospy.Rate(10)
		
		#Read in the raw image data from the kinect
		imageSubscriber = rospy.Subscriber("turtlebot0/camera/rgb/image_raw", Image, self.imageCallback)
		depthSubscriber = rospy.Subscriber("turtlebot0/camera/depth/image_raw", Image, self.depthCallback)
		pointSubscriber = rospy.Subscriber("turtlebot0/camera/depth/points", PointCloud2, self.pointCloudCallback)
		
		#Subscribe to the boundingsquares topic published by the clusterer
		squaresSubscriber = rospy.Subscriber("turtlebot0/boundingSquares", bounding_squares, self.boundingSquaresCallback)
		
		#Initiate the image feeds
		self.latestPoints = PointCloud2()
		self.latestImage = Image()
		self.latestDepthImage = Image()
		self.latestBoundingSquares = bounding_squares()
		self.imagePublished = False
		self.newImagePublished = False
		
		#Create a directory to save images in, each image set should go in its own uniquely named folder
		baseFileDir = "../negative_image_sets/"
		folderBaseName = "set"
		self.fileDir = baseFileDir + folderBaseName + "/"
		
		#If this directory doesn't already exist then make it
		if not os.path.exists(self.fileDir):
			os.makedirs(self.fileDir)
		#Otherwise, add some number to the end to make the directory name unique
		else:
			identifier = 2	
			while os.path.exists(baseFileDir + folderBaseName + str(identifier)):
				identifier += 1
			self.fileDir = baseFileDir + folderBaseName + str(identifier) + "/"
			os.makedirs(self.fileDir)
		
		print "Created directory {} for images..".format(self.fileDir)
		
		#Variable to keep track of number of images in the dataset generated
		self.generatedImages = 0
		self.baseFilename = "img"
		
		#Create service proxies for spawning and deleting models
		try:
			self.worldPropertiesProxy = rospy.ServiceProxy("gazebo/get_world_properties", GetWorldProperties)
			self.physicsPropertiesProxy = rospy.ServiceProxy("gazebo/set_physics_properties", SetPhysicsProperties)
			self.spawnModelProxy = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
			self.pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
			self.unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
			self.removeProxy = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)
			self.configurationProxy = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)
			
		except Exception, e:
			print "Unable to set up service proxies"
			print e
		
		#Read in the XML for each model once at the start, faster than doing it over and over again unnecessarily
		self.model_xml = []
		for i in range(len(self.targetModelSet)):
			with open(self.targetModelSet[i], "r") as f:
				xml = f.read()
				self.model_xml.append(xml)
		
		self.clutter_xml = []
		for i in range(len(self.clutterModelSet)):
			with open(self.clutterModelSet[i], "r") as f:
				xml = f.read()
				self.clutter_xml.append(xml)
		
		with open(self.wallModelPath, "r") as f:
			self.wallXML = f.read()
		
		#Start the main loop
		while self.generatedImages < self.imageSetSize:
			if not rospy.is_shutdown():
				self.rotateTurtlebot()
				self.randomiseBackground()
				self.captureImage()
				self.captureImagesHough()
				self.removeModel()
			else:
				print "Keyboard Interrupt detected."
				sys.exit()
				
		print "Full image set generated."

		
			
	def rotateTurtlebot(self):
		#Spawns a random model from the selected set in a random location within the turtlebots field of view
		# this assumes a turtlebot is located at 0,0 with no rotation
		
		#Begin by rotating the robot around the z axis at random
		turtlebotRotation = (random.random()*math.pi*2)-math.pi
		self.currentRotation = turtlebotRotation 						#Store for access by other functions
		quaternion = quaternion_from_euler(0, 0, turtlebotRotation)		#Get a random rotation as a quaternion
		
		#Form a message to send to the service
		configurationMessage = ModelState()
		configurationMessage.model_name = "Turtlebot0"
		configurationMessage.pose.orientation.x = quaternion[0]
		configurationMessage.pose.orientation.y = quaternion[1]
		configurationMessage.pose.orientation.z = quaternion[2]
		configurationMessage.pose.orientation.w = quaternion[3]
		
		#Call the service with this message			
		self.configurationProxy(configurationMessage)
		
	def randomiseBackground(self):
		#Pick a random item of clutter (including turtlebots) (or no clutter at all)
		
		#With a probability of 0.25 we use another turtlebot as the object of clutter
		if random.random() < 0.25:
			usingTurtleBotAsClutter = True
		else:
			usingTurtleBotAsClutter = False
			
		clutterIndex = int(math.floor(random.random()*len(self.clutterModelSet)))
			
		clutterDistance = self.modelDistance + self.backgroundMinDistance + random.random()*(self.backgroundMaxDistance-self.backgroundMaxDistance)
		
		clutterOffset = ((random.random()*2)-1)*self.backgroundMaxOffset
		
		randomRotation = ((random.random()*2)-1)*math.pi
			
		#Convert the rotation into a quaternion
		quaternion = quaternion_from_euler(0, 0, randomRotation)
			
		#Figure out the right x and y to put it behind the target
		rotatedX = math.cos(self.currentRotation)*clutterDistance - math.sin(self.currentRotation)*clutterOffset 
		rotatedY = math.sin(self.currentRotation)*clutterDistance + math.cos(self.currentRotation)*clutterOffset
		
		#If we are using a turtlebot as the clutter, we move the second turtlebot in the world to the random position we just generated
		if usingTurtleBotAsClutter:
			
			#Create an empty modelstate to put the pose in
			turtleBotState = ModelState()
			turtleBotState.model_name = "Turtlebot1"
			turtleBotState.pose.position.x=rotatedX
			turtleBotState.pose.position.y=rotatedY
			turtleBotState.pose.orientation.y = quaternion[0]
			turtleBotState.pose.orientation.y = quaternion[1]
			turtleBotState.pose.orientation.z = quaternion[2]
			turtleBotState.pose.orientation.w = quaternion[3]
			
			#Pass this state to the service which moves objects
			if not self.configurationProxy(turtleBotState):
				print "Unable to relocate turtleBot"
		
		#Otherwise, we spawn an ordinary object at this location
		else:	
			#Format a spawn request
			request = SpawnModelRequest()
			request.initial_pose.position.x=rotatedX
			request.initial_pose.position.y=rotatedY
			request.initial_pose.position.z=0.0
			request.initial_pose.orientation.x = quaternion[0]
			request.initial_pose.orientation.y = quaternion[1]
			request.initial_pose.orientation.z = quaternion[2]
			request.initial_pose.orientation.w = quaternion[3]
			request.model_xml = self.clutter_xml[clutterIndex]
			request.model_name = "Clutter"
				
			#Call the proxy to spawn the target
			if not self.spawnModelProxy(request):
				print "Unable to spawn model."
			
		#Spawn a wall behind the target at some distance
		wallDistance = self.wallMinDistance + random.random()*(self.wallMaxDistance-self.wallMinDistance)
		
		#Convert the rotation into a quaternion
		quaternion = quaternion_from_euler(0, 0, (math.pi/2)+self.currentRotation)
			
		#Figure out the right x and y to put it behind the target
		rotatedX = math.cos(self.currentRotation)*(wallDistance+clutterDistance+self.modelDistance)
		rotatedY = math.sin(self.currentRotation)*(wallDistance+clutterDistance+self.modelDistance)
		
		#Format a spawn request
		request = SpawnModelRequest()
		request.initial_pose.position.x=rotatedX
		request.initial_pose.position.y=rotatedY
		request.initial_pose.position.z=0.0
		request.initial_pose.orientation.x = quaternion[0]
		request.initial_pose.orientation.y = quaternion[1]
		request.initial_pose.orientation.z = quaternion[2]
		request.initial_pose.orientation.w = quaternion[3]
		request.model_xml = self.wallXML
		request.model_name = "Wall"
		
		#Call the proxy to spawn the wall
		if not self.spawnModelProxy(request):
			print "Unable to spawn wall model."
		
		
	def removeModel(self):
		#Removes the model produced by spawnModel(), so that the scene is cleaned before the next image is captured
		#This is easily done, since we always assign the same name to the model which is all we need to know to remove it
		
		#Wait for the service and create a proxy
		rospy.wait_for_service("gazebo/delete_model")
		removeProxy = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)

		#Call the proxy to remove the item
		removeProxy("Target")
		
		#Also, try to remove the background clutter - may return false if none exists but thats fine
		removeProxy("Clutter")
		removeProxy("Wall")
		
		#Finally, reset the second turtlebot to the corner of the scene out of the way
		#Create an empty modelstate to put the pose in
		turtleBotState = ModelState()
		turtleBotState.model_name = "Turtlebot1"
		turtleBotState.pose.position.x=15
		turtleBotState.pose.position.y=15
			
		#Pass this state to the service which moves objects
		if not self.configurationProxy(turtleBotState):
			print "Unable to relocate turtleBot"
		
		
		
	def captureImage(self):
		#Captures an image of the spawned model 
		
		#First, wait for a new image to be published to make sure that the newly spawned model is actually visible in the camera feed
		
		while not self.newImagePublished:
			self.tenHzTimer.sleep()
			
		#Reset the variable to false before moving on
		self.newImagePublished = False
				
		#From the bounding squares topic we may get a number of objects, but we only want the square that contains the object 
		for i in range(20):
			self.tenHzTimer.sleep()
		squares = self.latestBoundingSquares
		
		#Wait for at least one bounding square to be found in the image, giving up after a certain time
		maxWait = 10
		waitCount = 0
		while squares.count == 0:
			self.tenHzTimer.sleep()
			waitCount += 1
			if waitCount >= maxWait:
				print "Unable to find any bounding squares in image - moving on without capturing"
				return
			else:
				squares = self.latestBoundingSquares
		
		#Search through all the squares to find the one that is closest in the depth field - that is our area to crop out
		# check the depth feed value for the central pixel in that square
		closestSquareIndex = 0
			
		if squares.count > 1:	#If more than one is found, we go into this little loop to find the closest
			#print "{} squares found.".format(squares.count)
			#Convert the latest depth image to a usable format
			depthImageConverted = self.bridge.imgmsg_to_cv2(self.latestDepthImage)
				
			#Find the square with the lowest depth value for its central pixel
			closestSquareDistance = 999
			for i in range(squares.count):
				cx = (squares.left_pixel_indexes[i] + (squares.side_lengths[i]/2))
				cy = (squares.top_pixel_indexes[i] + (squares.side_lengths[i]/2))
				#Get these pixel values for the non-downsampled image
				scalingFactor = 0.25
				cx /= scalingFactor
				cy /= scalingFactor
				#print "Cluster {} centre ({},{}) value {}".format(i, cx, cy, depthImageConverted[cx][cy])
				if float(depthImageConverted[cx][cy]) < closestSquareDistance:
					closestSquareIndex = i
					closestSquareDistance = depthImageConverted[cx][cy]
			
		#Crop image to this square alone
		#Calculate start and end pixels IN THE DOWNSAMPLED IMAGE
		xStart = squares.left_pixel_indexes[closestSquareIndex]
		xEnd = squares.left_pixel_indexes[closestSquareIndex] + squares.side_lengths[closestSquareIndex]
		yStart = squares.top_pixel_indexes[closestSquareIndex]
		yEnd = squares.top_pixel_indexes[closestSquareIndex] + squares.side_lengths[closestSquareIndex]
		
		#Get the latest RGB image via cvbridge
		try:
			#Convert the raw image into a format readable by openCV; then convert rgb to hue, saturation, value 
			convertedImage = self.bridge.imgmsg_to_cv2(self.latestImage)
		except Exception as e:
			print "Unable to convert raw image to hsv"
			print e
			
		#Convert these coordinates to work in the undownsampled image
		scalingFactor = 0.25
		xStart *= 1/scalingFactor
		xEnd *= 1/scalingFactor
		yStart *= 1/scalingFactor
		yEnd *= 1/scalingFactor
		croppedImage = convertedImage[xStart:xEnd, yStart:yEnd]
			
		#Resize the image for the classifier
		desiredSize = (32,32)
		resizedImage = cv2.resize(croppedImage, desiredSize, interpolation=cv2.INTER_LINEAR)

		#Generate a filename and check that it's unique
		fileName = self.baseFilename + str(self.generatedImages)
			
		if not os.path.isfile(self.fileDir + fileName):
			#Save the image
			cv2.imwrite("{}.png".format(self.fileDir+fileName), resizedImage)
			print "Created image {} in {}..".format(fileName, self.fileDir)
		else:
			print "Problem writing image, {} already exists in {}..".format(fileName, self.fileDir)
			
		#Increment the count of generated images
		self.generatedImages += 1
		
		#Now, capture another image from some random region in the image
		
		#Randomly pick a topleft corner for our region
		randomLeftmostIndex = int(math.floor(random.random()*convertedImage.shape[0]))
		randomTopmostIndex = int(math.floor(random.random()*convertedImage.shape[1]))
		
		#Choose a side length that will definitely keep the region within the image
		rightMargin = convertedImage.shape[0]-randomLeftmostIndex
		bottomMargin = convertedImage.shape[1]-randomTopmostIndex
		
		#Make sure the side length is shorter than the smallest of the two margins
		if rightMargin > bottomMargin:
			randomSideLength = int(math.ceil(random.random()*bottomMargin))
		else:
			randomSideLength = int(math.ceil(random.random()*rightMargin))
		
		#Now, crop out this region from the camera feed
		croppedRandomImage = convertedImage[randomLeftmostIndex:randomLeftmostIndex+randomSideLength, randomTopmostIndex:randomTopmostIndex+randomSideLength]
		#print convertedImage.shape
		#print randomLeftmostIndex, randomTopmostIndex, randomSideLength, croppedRandomImage.shape
		#Resize the image for the classifier
		resizedRandomImage = cv2.resize(croppedRandomImage, desiredSize, interpolation=cv2.INTER_LINEAR)

		#Generate a filename and check that it's unique
		fileName = self.baseFilename + str(self.generatedImages)
			
		if not os.path.isfile(self.fileDir + fileName):
			#Save the image
			cv2.imwrite("{}.png".format(self.fileDir+fileName), resizedRandomImage)
			print "Created image {} in {}..".format(fileName, self.fileDir)
		else:
			print "Problem writing image, {} already exists in {}..".format(fileName, self.fileDir)
			
		#Increment the counter of generated images
		self.generatedImages += 1
	
	def captureImagesHough(self):
		#Additionally, we use the hough transform system from "neural_target_filter_node.py" to choose some regions to capture images from
		#This code is largely copied from neural_target_filter_node.py (originally written by me though)
		
		#Define the sizes of the squares to look for targets in and how many of each to capture
		squareSizes = [10, 20, 30]
		maxRegionsChecked = [2, 2, 2]
		
		
		#First, check that an image has been published
		if self.imagePublished:
			
			#Begin by converting our image to a usable format, this also prevents it changing while we are using it
			try:
				convertedImage = self.bridge.imgmsg_to_cv2(self.latestImage)
				
				#Produce a downsampled version of the image for performance and convert it to grayscale so that it can be fed into the edge detector
				downsampledImage = cv2.resize(convertedImage, (0,0), fx=self.downsamplingFactor, fy=self.downsamplingFactor, interpolation=cv2.INTER_NEAREST)
				downsampledImage = cv2.cvtColor(downsampledImage, cv2.COLOR_BGR2GRAY)
			except Exception as e:
				print "Target filter node unable to convert image message to usable format"
				print e
		
			#Generate thresholds for edge detection based on otsu's method
			upperThreshold, thresh = cv2.threshold(downsampledImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
			lowerThreshold = 0.5*upperThreshold
			
			#Perform edge detection on the resulting image, using canny edge detector
			kernelSize = 3	#Use default kernel size
			edgeImage = cv2.Canny(downsampledImage, lowerThreshold, upperThreshold)
			
			#Now for the next step we perform a hough transform to identify squares in the image with high concentrations of edge pixels
			
			#begin by defining a data structure for the parameter space of the hough transform
			#discretisation factor defines how much smaller the parameter space is than the image itself
			discretisationFactor = 0.2
			parameterSpaceX = int(edgeImage.shape[0]*discretisationFactor)
			parameterSpaceY = int(edgeImage.shape[1]*discretisationFactor)
			parameterSpaceZ = len(squareSizes)
			accumulator = np.zeros((parameterSpaceX, parameterSpaceY, parameterSpaceZ))
			
			#Now, go through every pixel in the edge image
			for i in range(edgeImage.shape[0]):
				for j in range(edgeImage.shape[1]):
					
					#Check if the pixel is non-zero, meaning it is part of an edge
					if edgeImage[i][j] != 0:
						
						#Vote for every point in the parameter space which describes a square that contains this pixel
						#Check for each of the possible square sizes in turn
						for k in range(parameterSpaceZ):
							
							sideLength = squareSizes[k]
							
							#Calculate the set of points for which, a square of this size with its top left corner at that point would contain this coordinate
							votingRegionLeftBoundary = j-sideLength
							votingRegionTopBoundary = i-sideLength
							
							#Avoid array index errors, by never letting the top or left indexes be below zero
							if votingRegionLeftBoundary < 0:
								votingRegionLeftBoundary = 0
							if votingRegionTopBoundary < 0:
								votingRegionTopBoundary = 0
								
							#Define the right and bottom boundary of the voting region at this coordinate, since no square with its top left corner below 
							# or to the right of this coordinate will contain it.
							votingRegionRightBoundary = j
							votingRegionBottomBoundary = i
							
							#Now, calculate the area within the parameter space to vote for, since it is smaller than the image itself
							parameterSpaceLeftBoundary = int(math.floor(votingRegionLeftBoundary*discretisationFactor))
							parameterSpaceRightBoundary = int(math.ceil(votingRegionLeftBoundary*discretisationFactor))
							parameterSpaceTopBoundary = int(math.floor(votingRegionTopBoundary*discretisationFactor))
							parameterSpaceBottomBoundary = int(math.floor(votingRegionBottomBoundary*discretisationFactor))
							
							#For each point within this region in the parameter space, assign one vote
							accumulator[parameterSpaceTopBoundary:parameterSpaceBottomBoundary, parameterSpaceLeftBoundary:parameterSpaceRightBoundary, k] += 1
										
			#Calculate the areas of each size of square to save us repeatedly needing to do so
			squareAreas = []
			for i in range(len(squareSizes)):
				squareAreas.append(squareSizes[i]**2)
				
			#From the parameter space, see which sets of parameters give us regions with the highest concentration of edge pixels - these are the regions we will check with the neural classifier
			
			#Keep track of interesting regions here, seperated by size since we want a few of each size
			regionsToCheck = []
			for i in range(parameterSpaceZ):
				regionsToCheck.append([])
			
			#Now, look at each possible x,y position to place a square in the parameter space 
			for x in range(parameterSpaceX):
				for y in range(parameterSpaceY):
						
					for z in range(parameterSpaceZ):
						
						#Calculate this accumulator's activation over the size of the area it relates to
						activation = accumulator[x][y][z]
						activationOverArea = float(activation)/squareAreas[z]
						
						#If this region contains some edge pixels then we add it to the list of potential regions to check
						if activation > 0:
							
							#Calculate the pixel coordinates of this x,y in the downsampled image
							imageXCoord = int(x/discretisationFactor)
							imageYCoord = int(y/discretisationFactor)
							
							#Add this square to the list of image regions to check
							regionsToCheck[z].append((imageXCoord, imageYCoord, squareSizes[z], activationOverArea))
							
			
			#Now, select a few regions of each size here, so that regions of a given size are non-overlapping, with a set number selected of each size
			selectedRegions = []
			for i in range(parameterSpaceZ):	
				#Sort according to their activation over area, since these are the ones we want to sample first
				regionsToCheck[i].sort(key=lambda x: x[3], reverse=True)
								
				#Keep track of how many we've selected
				checkedCount = 0
				previouslyCheckedRegions = []
							
				#Look at each region in the list
				for region in regionsToCheck[i]:
					
					#Make sure we haven't already checked the maximum number of regions
					if checkedCount < maxRegionsChecked[i]:
						
						#Check this region doesn't overlap with any previously checked regions of the same size
						overlapping = False
						for previous in previouslyCheckedRegions:
							if self.regionsOverlap(previous, region):
								overlapping = True
						
						#If it doesn't overlap, then we add this to the list of regions to capture and increment the count of checked regions
						if not overlapping:
							checkedCount += 1
							previouslyCheckedRegions.append((region[0], region[1], region[2]))		#Store a tuple describing the region for comparison later (to avoid overlapping)
							selectedRegions.append((region[0], region[1], region[2]))
							
		#Now that we've chosen a set of regions, capture an image of each and save it for inclusion in the training set
		for region in selectedRegions:
			
			#Calculate the area of the non-downsampled image to crop out
			scaledXStart = int(region[0]/self.downsamplingFactor)
			scaledYStart = int(region[1]/self.downsamplingFactor)
			scaledXEnd = int((region[0]+region[2])/self.downsamplingFactor)
			scaledYEnd = int((region[1]+region[2])/self.downsamplingFactor)
			
			#Crop out the relevant area
			croppedRegion = convertedImage[scaledXStart:scaledXEnd, scaledYStart:scaledYEnd]
			
			#Resize the image to our desired size for the neural network
			desiredSize = (32, 32)
			resizedRandomImage = cv2.resize(croppedRegion, desiredSize, interpolation=cv2.INTER_LINEAR)

			#Generate a filename and check that it's unique
			fileName = self.baseFilename + str(self.generatedImages)
				
			if not os.path.isfile(self.fileDir + fileName):
				#Save the image
				cv2.imwrite("{}.png".format(self.fileDir+fileName), resizedRandomImage)
				print "Created image {} in {}..".format(fileName, self.fileDir)
			else:
				print "Problem writing image, {} already exists in {}..".format(fileName, self.fileDir)
				
			#Increment the counter of generated images
			self.generatedImages += 1
		
	def regionsOverlap(self, a, b):
		#Takes two square regions defined by the coordinates of the top left pixel and side length,
		# then returns true or false for if those regions overlap
			
		xOverlapping, yOverlapping = False, False
		
		#Calculate the left right top and bottom sides of each square
		aL, bL = a[0], b[0]
		aR, bR = a[0]+a[2], b[0]+b[2]
		aB, bB = a[1], b[1]
		aT, bT = a[1]+a[2], a[1]+a[2]
		
		#Check if a is either to the left or the right of B
		if not ((aL > bR) or (aR < bL)):
			xOverlapping = True
		
		#Now check if a is either below or above B
		if not ((aT < bB) or (aB > bT)):
			yOverlapping = True
		
		#If the squares overlap in the x and y axis then they overlap spatially
		if xOverlapping and yOverlapping:
			return True
		else:
			return False
							
	def pointCloudCallback(self, points):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestPoints = points
		
	def imageCallback(self, image):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestImage = image
		self.imagePublished = True
		self.newImagePublished = True
		
	def boundingSquaresCallback(self, squaresMessage):
		#Just store the latest set of squares for use elsewhere
		self.latestBoundingSquares = squaresMessage
		
	def depthCallback(self, image):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestDepthImage = image
		
def main(args):
	imageGenerator = ImageSetGenerator()
	
if __name__=="__main__":
	main(sys.argv)
