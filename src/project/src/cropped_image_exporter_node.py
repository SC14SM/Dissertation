#!/usr/bin/env python

# Listens to a topic which publishes bounding squares of objects detected in the depth feed, then takes an image of this object from the rgb
# camera feed and publishes it to be saved by another node for use as training data in the image recognition system.

import rospy
import cv2
import numpy as np
import sys

from project.msg import bounding_squares
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class cropped_image_exporter():
	
	def __init__(self):
		
		
		#Initiate the ROS node
		rospy.init_node("Depth_Image_Clusterer", anonymous = True)
		
		#Instantiate the cv bridge to convert the image for openCV
		self.bridge = CvBridge()
		
		#Create a publisher for the cropped image
		self.croppedImagePublisher = rospy.Publisher("croppedImages", Image, queue_size = 3)
		
		#Create a timer, so that images are only published with some maximum frequency
		maxFrequency = 10
		tenHz = rospy.Rate(maxFrequency)
		
		#Create a subscriber to the RGB camera feed, and the boundingSquares feed
		imageSubscriber = rospy.Subscriber("camera/rgb/image_raw", Image, self.imageCallback)
		squaresSubscriber = rospy.Subscriber("boundingSquares", bounding_squares, self.squaresCallback)
		
		self.receivedImage = False

		#Wait for an image to be received:
		while (not rospy.is_shutdown()) and (not self.receivedImage):
			tenHz.sleep()
			
		def imageCallback(self, image):
			#Simply store the image, so that the callback function doesn't get bogged down and fall behind
			self.latestImage = image
			#At some point earlier on I got height and width switched around, so these next two lines are as intended even if it seems odd
			self.imageWidth = image.height
			self.imageHeight = image.width
			self.receivedImage = True
			
		def squaresCallback(self, squares):
			
			#Define the degree to which the depth image has been downsampled, which is necessary to find the indexes on the full size image
			scaleFactor
			
			#Check that at least one camera image has been received
			if self.receivedImage:
				
				#Convert image to a format useable by opencv
				try:
				#Convert the image into openCV format
					convertedImage = self.bridge.imgmsg_to_cv2(Image)
			
				except Exception, e:
					print "unable to convert depth image to bgr"
					print e
					
				#For each bounding square in the message:
				croppedImages = []
				
				for i in range(squares.count):
					
					#Crop out the relevant square from the whole rgb image, remembering that the indexes are from a downsampled version
					topStartIndex = squares.top_pixel_indexes[i]/scaleFactor
					leftStartIndex = squares.left_pixel_indexes[i]/scaleFactor
					sideLength = squares.side_lengths[i]/scaleFactor
					
					croppedImage = convertedImage[topStartIndex:topStartIndex+sideLength, leftStartIndex:leftStartIndex+sideLength]
					
					#Downsample the cropped segment to the desired size for the object recognition node
					desiredSize = (32,32)
					downsampledImage = cv2.resize(imageU8, desiredSize, interpolation=cv2.INTER_LINEAR)

					#Add this to our list
					croppedImages.append(downsampledImage)
				
				#Having produced our cropped images, we package them into a message for publishing
				outputMessage = image_bundle()
				
				outputMessage.images = croppedImages
				
				#We also include the info from the bounding squares that produced each image
				# so that if the cropped image is found to contain an object, we know what part of the camera feed said object is in
				outputMessage.top_pixel_indexes = squares.top_pixel_indexes
				outputMessage.left_pixel_indexes = squares.left_pixel_indexes
				outputMessage.side_lengths = squares.side_lengths
				
				#Publish the bundle of images
				self.imagePublisher.publish(outputMessage)
				
					
					
					
				
			
