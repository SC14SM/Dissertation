#!/usr/bin/env python

import rospy
import cv2
import numpy as np
import sys
import math

from std_msgs.msg import Bool
from geometry_msgs.msg import Twist, Vector3


#	Class to control a single turtlebot during the deliberative search. Takes the outputs of various nodes and forms a single consistent set of behaviours.
# 	Behaviour is pretty simple: It follows its guidance vector (published by the master node) along a pre-planned route, only leaving the route if a target is 
# 	spotted within some distance, it then goes directly to the target, removes it and returns to its pre-planned route.

#	Presumes the existence of several other nodes:
#	-Some sort of detector node publishing to target_vector
#	-Some sort of avoidance node publishing to laser_avoidance
#	-Some sort of grabber node listening on grabber_trigger and publishing to grabber_return
#	-Some sort of movement node publishing on smooth_movement_commands and listening on movement_vector
#	-Some sort of guidance node publishing on guidance_vector

#	This class is not concerned with how these nodes are actually implemented, for instance any one of several movement types can easily be swapped in or out without affecting the 
# 	operation of this node. Likewise, although this node was written with a simplified version of the target detection in place, a more sophisticated detector node can easily
#	be swapped in during the launch process without any adjustments needed to this code.

class controlNode():
	
	def __init__(self, args):
		
		#Start by initiating the ros node
		rospy.init_node("Control_node", anonymous = True)
				
		#Some variables determining how close a piece of rubbish has to be for the turtlebot to leave its route and pick it up
		self.detectionThreshold = 2.0	
		self.pickupThreshold 	= 0.5
		
		#Create a timer, specifying how often to process all the vectors - our system doesn't move fast enough to need more than a few per second
		maxUpdateFrequency = 5
		self.updateTimer = rospy.Rate(maxUpdateFrequency)
		
		#Initialise variables for the subscribers
		self.latestDetectorVector = Vector3()
		self.latestAvoidanceVector = Vector3()
		self.latestGuidanceVector = Vector3()
		self.latestMovementMessage = Twist()
		self.grabberDone = False

		#Create subscribers to these nodes we've just created, and to the vector that is published by the master node
		detectorSubscriber = rospy.Subscriber("target_vector", Vector3, self.detectorCallback)
		avoidanceSubscriber = rospy.Subscriber("laser_avoidance_vector", Vector3, self.avoidanceCallback)
		guidanceSubscriber = rospy.Subscriber("guidance_vector", Vector3, self.guidanceCallback)
		grabberSubscriber = rospy.Subscriber("grabber_return", Bool, self.grabberCallback)
		movementSubscriber = rospy.Subscriber("movement_commands", Twist, self.movementCallback)
		
		#Create a publisher to communicate with the base and grabber arm
		self.movementPublisher = rospy.Publisher("movement_vector", Vector3, queue_size=5)
		self.basePublisher = rospy.Publisher("mobile_base/commands/velocity", Twist, queue_size=5)
		self.grabberPublisher = rospy.Publisher("grabber_trigger", Vector3, queue_size=5)
		
		#Finally, start the main loop
		self.startControlLoop()
		
	def startControlLoop(self):
		#Handle the main bulk of the control logic
		
		while True:
			
			#Calculate magnitude of detection vector
			targetDistance = math.sqrt(math.pow(self.latestDetectorVector.x, 2) + math.pow(self.latestDetectorVector.y,2))
			
			#Check if dectection is within threshold - if it equals zero then nothing has been detected so we go to the other clause
			if (targetDistance!=0.0) and (targetDistance <= self.detectionThreshold):
				if targetDistance <= self.pickupThreshold:
					self.pickup(self.latestDetectorVector)
				else:
					#Move towards the target, taking the avoidance vector into account
					vectorSum = Vector3((self.latestDetectorVector.x+self.latestAvoidanceVector.x), (self.latestDetectorVector.y+self.latestAvoidanceVector.y), 0)
					self.movementPublisher.publish(vectorSum)
			else:
				#Move according to the guidance from the master node, plus avoidance
				vectorSum = Vector3((self.latestGuidanceVector.x+self.latestAvoidanceVector.x), (self.latestGuidanceVector.y+self.latestAvoidanceVector.y), 0)
				self.movementPublisher.publish(vectorSum)
				#Calculate a resulting movement vector
			
			#Now publish the actual movement commands generated from the movement node to the base, 
			self.basePublisher.publish(self.latestMovementMessage)
			
			#Sleep, so that we don't waste resources updating more frequently than we need to
			self.updateTimer.sleep()
	
	def pickup(self, vector):
		#Causes the turtlebot to remove a piece of rubbish in a small area in front of it
		
		#Publish an empty movement vector to stop the base for now
		self.basePublisher.publish(Twist())
		
		#Wait some time to reflect the non-instant operation time of a real grabber arm
		rospy.sleep(3)
		
		#Call the grabber arm to remove the object
		self.grabberPublisher.publish(vector)
		
		#Reset the detection vector so the TurtleBot knows the target in front of it is no longer present
		self.latestDetectorVector = Vector3()
		
	
	def detectorCallback(self, message):
		#Store the latest vector for use by the control loop
		self.latestDetectorVector = message
		
	def avoidanceCallback(self, message):
		#Store the latest vector for use by the control loop
		self.latestAvoidanceVector = message

	def guidanceCallback(self, message):
		#Store the latest vector for use by the control loop
		self.latestGuidanceVector = message

	def grabberCallback(self, message):
		#Set the flag for grabber finishing its routine
		self.grabberDone = True
	
	def movementCallback(self, message):
		#Store the latest twist message for use by the control loop
		self.latestMovementMessage = message
		
def main(args):
	control = controlNode(args)
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)
		
