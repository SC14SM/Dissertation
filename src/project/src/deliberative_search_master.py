#!/usr/bin/env python

# 	Master node for a deliberative search of an empty rectangular space. Used to gain a benchmark for optimal search times.
# 	Works by subdividing a rectangular area into a number of smaller rectangles depending on how many robots are available,
# 	then assigning each robot to a subarea and guiding it in a uniform search of this space
# 	Guiding is done by reading robot position from the gazebo world services (analogous to gps tracking in a real world scenario)
# 	and publishing a vector pointing towards the next goal point in a pre-planned route

import numpy as np
import rospy
import sys
import math 
import tf

from std_msgs.msg import Bool
from decimal import Decimal
from geometry_msgs.msg import Point, Vector3
from gazebo_msgs.srv import GetModelState

class deliberativeSearchMaster():
	
	publishPoints = True 	#Optionally publish the current destination as a world coordinate, for debugging
	
	def __init__(self, agents, xDimension, yDimension):
		
		#Begin by initiating the ros node
		rospy.init_node("deliberative_search_master", anonymous = True)
		
		#Cast arguments to the appropriate type from strings
		agents = int(agents)
		self.xDimension = float(xDimension)
		self.yDimension = float(yDimension)
		
		#Read in as an argument the number of turtlebots to be used
		self.agentCount = agents
		
		#Instantiate publishers for each of these, storing them in a list
		self.publishers = []
		if self.publishPoints:
			self.pointPublishers = []
		
		#Add in a subscriber to listen for a reset signal
		resetSubscriber = rospy.Subscriber("deliberativeResetSignal", Bool, self.resetCallback)
		
		self.modelNames = []
		
		for i in range(agents):
			#Generate a topic name for each robot
			topicName = "turtlebot{}/guidance_".format(i)
			
			#Instantiate a publisher and add it to the list
			self.publishers.append(rospy.Publisher(topicName+"vector", Vector3, queue_size = 5))
			if self.publishPoints:
				self.pointPublishers.append(rospy.Publisher(topicName+"point", Point, queue_size = 5))
			
			#Populate a list of model names
			self.modelNames.append("Turtlebot{}".format(i))
			
		#Create proxy for model state service
		rospy.wait_for_service("gazebo/get_world_properties")
		try:
			self.modelStateProxy = rospy.ServiceProxy("gazebo/get_model_state", GetModelState)
		
		except Exception, e:
			print "Problem creating service proxy", e		
		
		#Repeatedly start the control loop, by putting it in a while loop here we ensure that it will restart when the reset signal is received
		while not rospy.is_shutdown():
			self.runControlLoop()
		
	def runControlLoop(self):
		
		#Run an initial method to subdivide the map into regions and assign them to the turtlebots, then generate routes to explore these regions
		subregions = self.subdivideRegion(self.agentCount, self.xDimension, self.yDimension)
		self.destinations = self.generateDestinations(self.agentCount, subregions)
		
		#Initialise destination indexes to zero
		self.destinationIndexes = [0]*self.agentCount
		self.completedRoute = [False]*self.agentCount
		
		#Until a completion condition is met, run a control loop for the turtlebots, publishing directions for them to travel
		self.resetSignalDetected = False
		
		#Stop either if ros gets shutdown (generally keyboard interrupt via console) or if a reset signal is detected
		while not (self.resetSignalDetected or rospy.is_shutdown()):
			self.updateDestinationIndexes()
			self.generateAndPublishVectors()
	
	def resetCallback(self, message):
		#Stop the current loop and start a new one with the same settings, used for repeated testing without having to start up new sets of nodes
		self.resetSignalDetected = True
		print "Reset signal received by master node"
		
	class Region():
		#Data structure holding some parameters of a region
		def __init__(self, xOffset, xDimension, yOffset, yDimension):
			self.xOffset = xOffset
			self.xDimension = xDimension
			self.yOffset = yOffset
			self.yDimension = yDimension
		
	def subdivideRegion(self, agents, xDimension, yDimension):
		#Divides a rectangular region into a set number of equally sized rectangular sub-regions, centred on the point 0,0
		
		area = self.Region((-xDimension/2.0), xDimension, (-yDimension/2.0), yDimension)
		
		def recursiveDivide(agents, region):
			#Recursive function to divide the region into subregions
			subregions = []
			
			#If agent count is divisible by two then split the region along its longest dimension and call the function on each subregion
			if agents % 2 == 0:
				if region.xDimension > region.yDimension:
					a = self.Region(region.xOffset, (region.xDimension/2.0), region.yOffset, region.yDimension)
					b = self.Region((region.xOffset + (region.xDimension/2.0)), (region.xDimension/2.0), region.yOffset, region.yDimension)
				else:
					try:
						a = self.Region(region.xOffset, region.xDimension, region.yOffset, (region.yDimension/2.0))
						b = self.Region(region.xOffset, region.xDimension, (region.yOffset + (region.yDimension/2.0)), region.yDimension / 2.0)
					except Exception, e:
						print region.xOffset, region.xDimension, region.yOffset, region.yDimension
						
				#Recursively call the same function on the two halves of this region	 
				subregions += recursiveDivide(agents/2, a)
				subregions += recursiveDivide(agents/2, b)
				
			#If agent count is not divisible by two then split uniformly along the longest region
			else:
				if region.xDimension > region.yDimension:
					for i in range(agents):
						width = region.xDimension/agents
						offset = region.xOffset + i*width
						subregion = self.Region(offset, width, region.yOffset, region.yDimension)
						subregions.append(subregion)
				else:
					for i in range(agents):
						width = region.yDimension/agents
						offset = region.yOffset + i*width
						subregion = self.Region(region.xOffset, region.xDimension, offset, width)
						subregions.append(subregion)
			
			#return the list of generated subregions
			return subregions
		
		return recursiveDivide(agents, area)
			
	def generateDestinations(self, agents, subregions):
		#For each turtlebot, generate the set of points that make up its route
		
		destinations = [[]for i in range(agents)]
		
		maxPassWidth = 2.0			#The "width" of each pass in metres, determining how closely packed the various sweeps of the space are
		maxStepDistance = 0.5		#The distance between each destination on the route
		
		#Start by working out how many passes we are going to need and how many steps in each pass
		requiredPasses = int(math.ceil(subregions[0].yDimension/maxPassWidth))
		requiredSteps = int(math.ceil(subregions[0].xDimension/maxStepDistance))
		
		#Calculate the y distance between each pass, and the x distance between each step
		actualPassWidth = subregions[0].yDimension/requiredPasses
		actualStepDistance = subregions[0].xDimension/requiredSteps
		for i in range(agents):
			
			#Figure out which corner of the region is closest to 0,0 (the starting point)
			# make this the first destination
			
			if subregions[i].xOffset >= 0:
				startX = subregions[i].xOffset+(actualStepDistance/2.0)
				sweepDirection = 1
			else:
				startX = subregions[i].xOffset+subregions[i].xDimension-(actualStepDistance/2)
				sweepDirection = -1
				
			if subregions[i].yOffset >= 0:
				startY = subregions[i].yOffset+(actualPassWidth/2.0)
				yDir = 1
			else:
				startY = subregions[i].yOffset+subregions[i].yDimension-(actualPassWidth/2)
				yDir = -1
				
			#Start z is always equal to 0
			latestPoint = Point(startX, startY, 0)
			destinations[i].append(latestPoint)
			
			for sweep in range(requiredPasses):
				for step in range(requiredSteps-1):
					#Sweep along the x axis in steps of actualStepDistance with direction xDir
					newX = latestPoint.x + actualStepDistance*sweepDirection
					newY = latestPoint.y
					latestPoint = Point(newX, newY, 0)
					destinations[i].append(latestPoint)
				
				#At the end of the sweep, increment the y dimension and change direction along the x axis
				#Unless this is the final sweep
				if sweep != (requiredPasses-1):
					newX = latestPoint.x
					newY = latestPoint.y + actualPassWidth*yDir
					latestPoint = Point(newX, newY, 0)
					destinations[i].append(latestPoint)
					sweepDirection *= -1
		
		#Return the 2d list of destinations
		return destinations
			
	def generateAndPublishVectors(self):
		#For each model in the simulation, publish a vector in its own coordinate frame pointing towards its next target point on the pre planned path
		
		for i in range(self.agentCount):
			
			if not self.completedRoute[i]:
		
				#Read current position in the world frame
				rospy.wait_for_service("gazebo/get_model_state")
				currentPosition = self.modelStateProxy(self.modelNames[i], "world")
				targetPoint = self.destinations[i][self.destinationIndexes[i]]
				
				#Calculate the vector to destination in world frame
				vectorWorldFrame = Vector3()
				vectorWorldFrame.x = targetPoint.x - currentPosition.pose.position.x
				vectorWorldFrame.y = targetPoint.y - currentPosition.pose.position.y
				
				#Rotate the vector to convert it into the turtlebot's coordinate system
				# Note, it is necessary to calculate rotation from the quaternion system used by ROS, hence the long formula
				quaternion = (currentPosition.pose.orientation.x, currentPosition.pose.orientation.y, currentPosition.pose.orientation.z, currentPosition.pose.orientation.w)
				rollPitchYaw = tf.transformations.euler_from_quaternion(quaternion)
				
				rotation = rollPitchYaw[2]
			
				vectorTurtlebotFrame = Vector3()
				vectorTurtlebotFrame.x = (vectorWorldFrame.x*np.cos(-rotation) - vectorWorldFrame.y*np.sin(-rotation))
				vectorTurtlebotFrame.y = (vectorWorldFrame.x*np.sin(-rotation) + vectorWorldFrame.y*np.cos(-rotation))
				
				#Publish the resulting vector and optionally, the point in world coordinates that it is headed for
				self.publishers[i].publish(vectorTurtlebotFrame)
				if self.publishPoints:
					self.pointPublishers[i].publish(targetPoint)
			else:
				#If route is completed, publish an empty vector to get the turtlebot to stop
				self.publishers[i].publish(Vector3())
				
	def updateDestinationIndexes(self):
		#Checks the locations of each robot, and updates their next destination if they've reached their current 
		# these destinations when visited in order guide an individual turtlebot on a full traversal of its subregion in the space to be searched
		# the destinations are generated once at initialisation, then this method simply checks if the turtlebot has reached its current one and 
		# iterates through the list if it has.
		
		tolerance = 0.25		#Distance within which turtlebot must pass from destination to have "reached it", given in metres
		
		for i in range(self.agentCount):	#For each turtlebot
			
			if not self.completedRoute[i]:
				#Get current location from ros service call
				rospy.wait_for_service("gazebo/get_model_state")
				modelState = self.modelStateProxy(self.modelNames[i], "world")
				
				#Compare this location with the current destination, and if current location matches destination to within some tolerance, tell the turtlebot to move to the next destination in the list
				distance = math.sqrt(math.pow(modelState.pose.position.x-self.destinations[i][self.destinationIndexes[i]].x,2)+ math.pow(modelState.pose.position.y-self.destinations[i][self.destinationIndexes[i]].y,2))
				if distance < tolerance:
					if self.destinationIndexes[i] < (len(self.destinations[i])-1):
						self.destinationIndexes[i] += 1
					else:
						self.completedRoute[i] = True
						print "Agent {} completed route.".format(i)
				
		
def main(args):
	#Initiate the node, handing in the number of turtlebots as an argument in the form (number of agents, xwidth, ywidth)
	master = deliberativeSearchMaster(args[1], args[2], args[3])
	

if __name__=="__main__":
	main(sys.argv)
