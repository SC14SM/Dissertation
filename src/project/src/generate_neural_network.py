from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import cv2
import os

import matplotlib

def createModel():
	#Define the network shape as sequential since it's feedforward
	model = Sequential()
    
	#Set the input shape, for our 32x32, 3 channel images
	inputShape = (32, 32, 3)
    
	#Add our first set of convolution > pooling layers, with 32 filters
	model.add(Conv2D(32, (5, 5), padding='same', input_shape=inputShape))
	model.add(Activation("relu"))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2,2)))
	model.add(Dropout(0.25))
	
	#Add a second set, this time with 64 filters
	model.add(Conv2D(64, (5, 5), padding='same'))
	model.add(Activation("relu"))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2,2)))
	model.add(Dropout(0.25))
	
	#Add a final set, again with 64 filters
	model.add(Conv2D(64, (5, 5), padding='same'))
	model.add(Activation("relu"))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2,2)))
	model.add(Dropout(0.25))
	
	#Add a set of fully connected > RELU layers
	model.add(Flatten())
	model.add(Dense(512))
	model.add(Activation("relu"))
	model.add(Dropout(0.5))
	model.add(Dense(2))
	model.add(Activation("softmax"))
    
	return model
     
def trainNetwork():
	#Reads in the contents of all the subfolders in the training image folder to a single array
	
	datasetPath = "../training_data"
		
	imageFilePaths = sorted(list(paths.list_images(datasetPath)))
	
	random.shuffle(imageFilePaths)
	
	images = []
	labels = []

	for path in imageFilePaths:
		try:
			image = cv2.imread(path)
			image = img_to_array(image)
			images.append(image)
		except Exception as e:
			print path
			print e
			
		#Get the category of this image by taking the third to last element of its file path
		label = path.split(os.path.sep)[-3]
		if label == "positive_image_sets":
			labels.append(1)
		elif label == "negative_image_sets":
			labels.append(0)
		else:
			print "Unrecognised category, ensure images are split into positive and negative folders"
			
	#Convert the pixel values to the range 0..1 instead of 0..255
	images = np.array(images, dtype="float") / 255.0
	
	#Put the labels in a numpy array
	labels = np.array(labels)
	
	print labels
	
	#Split the data into training and testing
	(trainX, testX, trainY, testY) = train_test_split(images, labels, test_size = 0.1)
	
	#Get the label as a one-hot vector instead of an integer, for use by the neural net library
	trainY = to_categorical(trainY, num_classes=2)
	testY = to_categorical(testY, num_classes=2)
	
	#Create some extra data with an image generator, applying shears and crops etc. to pad out the training set
	dataAugment = ImageDataGenerator(rotation_range=30, width_shift_range=0.1,
		height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
		horizontal_flip=True, fill_mode="nearest")
	
	numEpochs = 5
	initialLearningRate = 1e-3
	batchSize = 64
	
	model = createModel()
	opt = Adam(lr=initialLearningRate, decay=initialLearningRate / numEpochs)
	model.compile(loss="binary_crossentropy", optimizer=opt,
		metrics=["accuracy"])
	 
	# train the network
	print("training network...")
	H = model.fit_generator(dataAugment.flow(trainX, trainY, batch_size=batchSize),
		validation_data=(testX, testY), steps_per_epoch=len(trainX) // batchSize,
		epochs=numEpochs, verbose=1)
	 
	# save the model structure to a JSON file
	print "Producing JSON structure output"
	model_json = model.to_json()
	with open("neuralNetworkStructure.json", "w") as outputJSON:
		outputJSON.write(model_json)
		
	#Now, save the weights in the model
	model.save_weights("neuralNetworkWeights.h5")
	print("Saving network weights to disk")

#Train the network
trainNetwork()
	
	
	
	
	
	
	
