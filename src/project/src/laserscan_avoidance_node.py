#!/usr/bin/env python

#	Node to handle short range obstacle avoidance via the laserscan topic.
#	Scans through the range values from the laserscan topic, and publishes a repulsive force away from any objects within
#	a certain radius of the scanner. This repulsive force scales exponentially as the distance to the object decreases,
#	and is weighted so that obstacles directly in front of the turtlebot generate a larger repulsive force than those to
#	one side, with obstacles beyond some angle being ignored completely (ie. obstacles behind the TurtleBot)

#	Used by every search strategy.

import numpy as np
import rospy
import sys
import math 

from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist, Vector3
from std_msgs.msg import String

class laserAvoidanceNode():

	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("laser_avoidance_node", anonymous=True)
		
		#Initiate publisher for the output
		self.vectorPublisher=rospy.Publisher("laser_avoidance_vector", Vector3, queue_size=5)
		
		#Subscribe to the laserscan topic
		laserSubscriber = rospy.Subscriber("laserscan", LaserScan, self.laserCallback)
		self.laserDataReceived = False
		
		#Subscribe to the parameter topic
		parameterSubscriber = rospy.Subscriber("parameters", String, self.parameterAdjustCallback) 
		
		#Initialise the avoidance radius and cutoff parameters to some default values
		self.avoidanceRadius = 0.5
		self.cutoff = 90
		
		#Repeatedly publish vectors at a maximum rate
		maxPublishRate = 4
		publishTimer = rospy.Rate(maxPublishRate)
		while not rospy.is_shutdown():
			self.publishVector()
			publishTimer.sleep()
		
		
	def parameterAdjustCallback(self, message):
		#Called when a message is received to adjust parameters
		
		#Start by splitting the string and taking only the entry that relates to this node
		splitString = message.data.split(",")
		self.avoidanceRadius = float(splitString[0])
		self.cutoff = float(splitString[1])
		
		print "Setting avoidance radius and cutoff to %.2f and %.2f" % (self.avoidanceRadius, self.cutoff)
		
	def laserCallback(self, laserScan):
		#Called every time a new set of laser ranges is received 
		#Simply store the laserscan data for use by another function
		self.laserDataReceived = True
		self.latestLaserData = laserScan
		
	def publishVector(self):
		#Publishes a single avoidance vector
		
		#First check that the subscriber has received some data
		if self.laserDataReceived:
				
			#Start by storing the data so it doesn't change while we are using it
			laserScan = self.latestLaserData
				
			#Define a vector to output
			resultantVector = Vector3()
			
			#Since we are not interested in readings that are outside the cutoff range we only take a slice of the rays		
			for i in range(self.indexFromAngle(-self.cutoff),self.indexFromAngle(self.cutoff)):
				#Get the laser scan range reading
				reading = laserScan.ranges[i]
				
				#We want to define a repulsive force that is asymptotal as distance approaches zero, so that a very close obstacle produces
				# and incredibly strong repulsion to avoid collision. For this we are using 1/(distance/avoidanceRadius-distance)
				# This has the advantage of ramping up smoothly from zero once the avoidance radius is reached, while meeting the first requirement
				if reading < self.avoidanceRadius:
					repulsion = 1/(reading/(self.avoidanceRadius-reading))
					
					#Calculate the counterclockwise rotation of this ray from the x axis
					angle = self.angleFromIndex(i)
					
					#Apply weighting according to how "central" the ray is, so that an obstacle directly in front of the turtlebot produces a larger
					# repulsive force than one off to the side. 
					# The weighting used is 1-|theta|/cutoff where cutoff is the point at which weighting reaches zero (in this case 90 degrees)
					weighting = 1-(abs(angle)/self.cutoff)
					
					#Add this vector to the final vector, which consists of the sum of all repulsion vectors
					#The vector is made negative so that the repulsion is in the opposite direction to the ray that produces it
					resultantVector.x += -repulsion*weighting*np.cos(np.deg2rad(angle))
					resultantVector.y += -repulsion*weighting*np.sin(np.deg2rad(angle))
					
			#Publish the summation of all the repulsion vectors
			magnitude = math.sqrt(math.pow(resultantVector.x, 2) + math.pow(resultantVector.y, 2))
			
			self.vectorPublisher.publish(resultantVector)
		
	def angleFromIndex(self, index):
		#Takes an index of a laser range, and returns the angle that that beam makes with the x axis of the robot (in degrees counterclockwise).
		totalRays = 1080
		angleIncrementPerRay = 0.25
		
		angle = (index-(totalRays/2))*angleIncrementPerRay
		
		return angle
	
	def indexFromAngle(self, angle):
		#Returns the array index of the laser ray that makes a specific angle with the x axis
		minAngle = -135
		angleIncrementPerRay = 0.25
		
		index = int(math.floor((angle - minAngle)/angleIncrementPerRay))
		
		return index
	
def main(args):
	node = laserAvoidanceNode()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)
		
