#!/usr/bin/env python

#	Node which listens to the output of another node, which produces a summation of the intensities in an image, and 
# 	publishes whether that output is descending or ascending, based on a rolling average over a window of time.

# 	Doesn't calculate the average over exactly the time window specified, it could vary slightly depending on how frequently
# 	messages are published, but given that many messages are published per second the window should closely reflect the 
# 	specified size.

#	This node is used in the reactive search strategy to increase the chance of making a random turn when the overall concentration
#	of targets in the field of view is decreasing.

import cv2
import numpy as np
import rospy
import sys

from std_msgs.msg import Float64, Bool

class gradient_calculator():
	
	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("gradient_calculator_node", anonymous=True)
		
		#Define a publisher for the output
		self.outputPublisher = rospy.Publisher("intensity_gradient", Float64, queue_size=5)
		
		#Initiate the latest reading
		self.latestIntensity = 0
		
		#Subscribe to the intensities and reset topics
		intensitySubscriber = rospy.Subscriber("summed_intensities", Float64, self.intensityCallback)
		resetSubscriber = rospy.Subscriber("reset", Bool, self.resetCallback)
		
		#Define a default value for the timewindow over which the average is calculated - in seconds
		self.timeWindow = 5
		
		#Also set a variable that ensures our average isn't calculated over EVERY image published in this window - otherwise we could be wasting 
		# resources working with hundreds of values that don't change very much
		self.maxReadingsPerSecond = 4 	
		self.previousAverage = 0
		self.lastReadingStoredTime = 0
		
		#Create a timer for the desired frequency
		self.timer = rospy.Rate(self.maxReadingsPerSecond)
		
		#Start a loop, continuously publishing gradients at the specified rate
		self.readings = []
		while not rospy.is_shutdown():
			self.updateRollingAverage()
			self.timer.sleep()
			
	def intensityCallback(self, reading):
		#Simply store the latest intensity received in a variable for use by other functions
		self.latestIntensity = reading.data
		
	def updateRollingAverage(self):
		#Update the rolling average of intensities and publish the change in average
		
		#If the intensity ever drops to zero then we just publish a change of -1 (the maximum negative change) to shift weighting to random movement
		if self.latestIntensity == 0:
			outputMessage = Float64()
			outputMessage.data = -1
			self.outputPublisher.publish(outputMessage)
		
		#Otherwise, we calculate the change in rolling average and publish that change, over the old average
		else:
			#Check if the array of readings is already of the desired size, if so remove the oldest reading
			if len(self.readings) >= self.maxReadingsPerSecond*self.timeWindow:
				self.readings.pop(0)
				
			#Add the new reading to the end of the list
			self.readings.append(self.latestIntensity)
				
			#Calculate the new average over the list and how this compares to the previous average
			newAverage = sum(self.readings)/len(self.readings)
				
			changeInAverage = newAverage - self.previousAverage
				
			#Publish the change over the previous average (much better than simply publishing raw change for various reasons detailed in report), and overwrite the previous average with the one we just calculated
			if self.previousAverage == 0:
				changeOverPrevious = changeInAverage
			else:
				changeOverPrevious = changeInAverage/self.previousAverage
			outputMessage = Float64()
			outputMessage.data = changeOverPrevious
			self.outputPublisher.publish(outputMessage)
			self.previousAverage = newAverage
		
	def parameterCallback(self, parameterMessage):
		#Used to adjust the parameters of the node programmatically
		pass
		
	def resetCallback(self, Bool):
		#Used to reset the rolling average whenever the testing resets
		self.readings = []
		self.previousAverage = 0
		
def main(args):
	#If this is the main path, launch an instance
	gradientCalc = gradient_calculator()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)		
		
		
