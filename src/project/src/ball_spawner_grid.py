#!/usr/bin/env python

#	Spawns green balls into the gazebo environment on a grid based layout,
#	with variable size. Generally used by the automated testing node.

import sys
import rospy
import math
import tf

from gazebo_msgs.srv import GetWorldProperties, SetPhysicsProperties, SpawnModel, SpawnModelRequest
from std_srvs.srv import Empty
from geometry_msgs.msg import *

def spawnGrid(areaX, areaY, rows, columns):

	#Wait for services to be ready
	rospy.wait_for_service("gazebo/get_world_properties")

	#Create service proxies
	try:
		worldPropertiesProxy = rospy.ServiceProxy("gazebo/get_world_properties", GetWorldProperties)
		physicsPropertiesProxy = rospy.ServiceProxy("gazebo/set_physics_properties", SetPhysicsProperties)
		spawnModelProxy = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
		pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
		unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
	except Exception, e:
		print "Unable to set up service proxies"
		print e
		
	#Now, calculate the x and y coordinates of each ball in the grid
	coords = []
	
	#Begin by calculating how far apart the rows and columns should be
	rowWidth = areaX/rows
	columnWidth = areaY/columns
	
	#Now log the coords of the intersection of each row and column
	
	startX = -(areaX*0.5)
	startY = -(areaY*0.5)
	
	for i in range(rows):
		for j in range(columns): 
			x = j*columnWidth + columnWidth/2.0
			y = i*rowWidth + rowWidth/2.0
			coords.append((x + startX,y + startY))
			
	#Read the xml for our model
	with open("greenSphere.sdf", "r") as f:
		ball_xml = f.read()
		
	#define an orientation
	orientation = Quaternion(0,0,0,0)

	#Now, spawn a ball at each of these coordinates
	for i in range(len(coords)):
		
		#Wait for service to respond, ensuring that requests don't get missed
		rospy.wait_for_service("gazebo/spawn_sdf_model")
		
		#Format a spawn request
		request = SpawnModelRequest()
		request.initial_pose.position.x=coords[i][0]
		request.initial_pose.position.y=coords[i][1]
		request.initial_pose.position.z=0.25
		request.model_xml = ball_xml
		request.model_name = "Ball" + str(i)
		
		#Try to spawn, printing a message if it fails
		if not spawnModelProxy(request):
			print "Unable to spawn model."

	print "Spawned models"	
