#!/usr/bin/env python

#	Node to mimic the behaviour of a grabbing arm or other rubbish removal mechanism in the system.
#	Uses calls to a ros service to check the area in front of the turtlebot for targets, and removes them from the simulation if so.
#	
#	Forms a crucial part of every search strategy

import sys
import rospy
import math

from std_msgs.msg import Bool
from gazebo_msgs.srv import GetWorldProperties, GetModelState, DeleteModel
from geometry_msgs.msg import Vector3

class grabberNode:
	
	def __init__(self, namespaceIndex):
		self.debug = True
		
		#As per usual, start by initiating the ros node
		rospy.init_node("Grabber_Node", anonymous=True)
		
		#Generate the model name
		self.name = "turtlebot{}".format(namespaceIndex)
		self.turtlebotModelName = "Turtlebot{}::base_footprint".format(namespaceIndex)

		#Create a subscriber, which listens on a topic until the node is called
		activationSubscriber = rospy.Subscriber("grabber_trigger", Vector3, self.callback)
		
		#Create a publisher, which reports when the "grabber" is done removing the target
		self.pub = rospy.Publisher("grabber_return", Bool, queue_size="8")
		
	def callback(self, vector):
		#Called when a message is received on the grabberTrigger topic
		
		#Begin by getting a list of all models in the gazebo world
		rospy.wait_for_service("/gazebo/get_world_properties")
		
		try:
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to call service", e
			return 0;		
		
		#Check if each model in the world is a potential target for pickup based on its model name
		for model in worldProperties.model_names:
			if self.targetName(model):
				
				#Get the location of this model
				modelStateProxy = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
				modelState = modelStateProxy(model, self.turtlebotModelName)
				
				#Check if it is in front of the turtlebot	
				if self.withinCircularRegion(modelState.pose):
					
					#If model is within region, remove it - then publish a message stating whether removal was successful
					returnMessage = Bool()
					if self.removeModel(model):
						returnMessage.data = True
					else:
						returnMessage.data = False
						
					print "{} removing {}...".format(self.name, model)
					self.pub.publish(returnMessage)
					
					#Return, since we only want to remove a single object at once
					return
					
		
	def test(self):
		#test method
		
		#Begin by getting a list of all models in the gazebo world
		rospy.wait_for_service("gazebo/get_world_properties")
		
		try:
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to call service", e
			return 0;		
		
		#Check if each model in the world is a potential target for pickup based on its model name
		for model in worldProperties.model_names:
			if self.targetName(model):
				print model, " is target"
				
				
				#Get the location of this model
				modelStateProxy = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
				modelState = modelStateProxy(model, self.turtlebotModelName)
				
				#Check if it is in front of the turtlebot	
				if self.withinCircularRegion(modelState.pose):
					#If model is within region, remove it - then publish a message stating whether removal was successful
					returnMessage = Bool()
					if self.removeModel(model):
						returnMessage.data = True
					else:
						returnMessage.data = False
						
					print "Removing ", model, "..."
					self.pub.publish(returnMessage)

	def withinCircularRegion(self, pose):
		#Checks if a given relative pose is in a circular region in front of the reference frame of the pose
		# In the context of a turtlebot reference frame, with radius 0.5m this checks if the point is in a circle of radius 0.5m centred 0.5m in front of the turtlebot 
		# [ignores z value]
		
		regionRadius = 0.4		#This value was reached by speculation about the likely range of a grabbing arm mounted on the turtlebot
		
		x = pose.position.x
		y = pose.position.y
				
		#Use pythagoras to check if it falls within a circular region
		if math.sqrt(pow(x-regionRadius, 2) + pow(y, 2)) < regionRadius:
			return True
		else:
			return False
		
	def targetName(self, name):
		#Checks if a given models name denotes it as a potential target for pickup
		# necessary because gazebo generates unique names for objects in the scene
		
		targetModelBaseNames = ["coke_can", "Ball", "cigarette_butt", "cigarette_box", "noodle_box", "styrofoam_box", "styrofoam_cup", "soda_bottle", "paper_ball"]		#List of target models, as opposed to models used as "obstacles"
		
		#Check if the base name appears in the name string, for instance "coke_can" would appear in "coke_can_3" denoting that it is a target
		for baseName in targetModelBaseNames:
			if name.find(baseName) != -1:
				return True
		
		#If not, return False
		return False
		
	def removeModel(self, name):
		#Removes the given model from the gazebo world, akin to picking it up with the grabbing arm being modelled
		removeProxy = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)
		
		#Delete it, and return the success message
		reply = removeProxy(name)	
		return reply.success
		
		
		
def main(args):
	grabber = grabberNode(args[1])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)
