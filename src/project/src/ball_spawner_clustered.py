#!/usr/bin/env python

#	Spawns green balls into the gazebo environment accodrind to a clustered layout.
#	Generally imported by the automated testing node, this script is able to spawn 
#	balls over a variable size area, with variable count and a variable "cluster parameter"
#	which defines how clustered the points are, with a parameter of zero giving us a uniform
#	random distribution and a parameter of 1 giving us a single large cluster

import sys
import rospy
import math
import tf
from random import random

from gazebo_msgs.srv import GetWorldProperties, SetPhysicsProperties, SpawnModel, SpawnModelRequest
from std_srvs.srv import Empty
from geometry_msgs.msg import *

def spawnRandom(ballCount, xDimension, yDimension, clusterParameter):
	
	#Begin by trying to generate a set of random points, repeating if necessary
	generatedPoints = 0
	while generatedPoints == 0:
		print "Trying to generate clustered distribution.."
		generatedPoints = generateRandomPoints(ballCount, xDimension, yDimension, clusterParameter)
	
	#Once we've successfully generated points (failure is unlikely but still possible if unlucky)
	print "Successfully generated distribution - spawning models.."
	spawnModels(generatedPoints)
	print "Finished spawning points."
	
def generateRandomPoints(ballCount, xDimension, yDimension, clusterParameter):
	#Some variables that describe the layout
	areaDimensions  =   [xDimension, yDimension]		#Dimensions of the rectangular area in which balls should be spawned	
	centreRadius = 2.5									#Keep a small circular region in the centre clear so that no balls spawn inside the turtlebots
	minimumBallDistance = 0.4							#Minimum distance the balls can be from eachother, stops weird physics results if one overlaps with another
	generatedSetFactor = 10								#The multiple of the desired number of balls that we generate, to sample from
	clusterDistance = 2									#Distance within which a nearby ball adds to "cluster score"
	clusterScoreWeighting = clusterParameter			#The balance between a uniform random and more clustered distribution
	
	
		
	#Generate a set of coordinates, checking that the balls are non-intersecting and don't fall within a region in the very centre where the robots will be

	def inAcceptableRegion(coords, r):
		#Returns true so long as the coords provided aren't within a circle of radius r centred at 0,0
		# or within 0.5 metres of the boundary
		
		if math.sqrt(pow(coords[0],2) + pow(coords[1],2)) < r:
			return False
		elif abs(coords[0]) > (areaDimensions[0]/2)-0.5 or abs(coords[1]) > (areaDimensions[1]/2)-0.5:
			return False
		else:
			return True

	def notIntersecting(newCoords, existingCoords, minimumDistance):
		#Checks that the new set of coords provided don't fall within a minimum distance of an existing set
		#returns true or false
		
		for existing in existingCoords:
			distance = math.sqrt(pow((newCoords[0]-existing[0]),2) + pow((newCoords[1]-existing[1]),2))
			if distance < minimumDistance:
				return False
		
		return True
		
	def clusterScore(coords, existingCoords, clusterDistance):
		#Scores a point according to how many points are within a certain distance of it and how close those points are
		score = 0
		for existing in existingCoords:
			distance = math.sqrt(pow((coords[0]-existing[0]),2) + pow((coords[1]-existing[1]),2))
			if distance < clusterDistance:
				score += clusterDistance/distance	
		return score

	#Now, produce the set of coords using the method we just wrote to check no points lie within the centre
	generatedPoints = []
	while len(generatedPoints) < ballCount*generatedSetFactor and not rospy.is_shutdown():
		newCoord = ((areaDimensions[0]*-0.5)+random()*areaDimensions[0], (areaDimensions[1]*-0.5)+random()*areaDimensions[1])
		if inAcceptableRegion(newCoord, centreRadius):
			generatedPoints.append(newCoord)		
	
	#Now, from this large set of generated points, we choose points according to how many other points are near them and already selected
	chosenPoints = []
	while len(chosenPoints) < ballCount and not rospy.is_shutdown():
		
		#Check the generated points has at least 1 entry left - if not then try again
		if len(generatedPoints) == 0:
			print "Failed to generate clustered layout from this set of random points- trying again.."
			return 0
		
		#Sum the total cluster score
		totalClusterScore = 0
		for point in generatedPoints:
			totalClusterScore += clusterScore(point, chosenPoints, clusterDistance)
		
		#Each point now has a value according to some weighting of its cluster score over total cluster score AND a standard base chance (both sum to 1)
		#We can choose a random point by generating a number in the range 0..1 and "counting" through the points 
		randomValue = random()
		
		#Find the point whose probability tips the summed probabilities over the random value we just picked
		index = -1		#Start at -1 so the first index we check is 0
		summedProbabilities = 0
		while summedProbabilities <= randomValue:
			#Check the next index
			index += 1
			
			#Base probability is the same for every point
			baseProbability = 1.0/len(generatedPoints)
			
			#Check total cluster score isn't zero here, to avoid errors - if it is then we just go by base chance
			if totalClusterScore != 0:
				clusterScoreProbability = clusterScore(generatedPoints[index], chosenPoints, clusterDistance)/totalClusterScore
				summedProbabilities += (clusterScoreWeighting*clusterScoreProbability + (1-clusterScoreWeighting)*baseProbability)
			else:
				summedProbabilities += baseProbability
		
		#Add this point to the set of chosen points provided it doesn't collide with any existing points
		if notIntersecting(generatedPoints[index], chosenPoints, minimumBallDistance):
			#Now, add this point to the set of chosen points and remove it from generated points
			chosenPoints.append(generatedPoints[index])
			generatedPoints.pop(index)
		else:
			generatedPoints.pop(index)
			
	#Having chosen our desired quantity of points, we return the array with their coordinates
	return chosenPoints
		
def spawnModels(coordSet):	
	#Spawns a number of green balls at the coordinate set passed in as an argument
	
	#Wait for services to be ready
	rospy.wait_for_service("gazebo/get_world_properties")

	#Create service proxies
	try:
		worldPropertiesProxy = rospy.ServiceProxy("gazebo/get_world_properties", GetWorldProperties)
		physicsPropertiesProxy = rospy.ServiceProxy("gazebo/set_physics_properties", SetPhysicsProperties)
		spawnModelProxy = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
		pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
		unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
	except Exception, e:
		print "Unable to set up service proxies"
		print e
	
	#Read the xml for our model
	with open("greenSphere.sdf", "r") as f:
		ball_xml = f.read()
		
	#define an orientation
	orientation = Quaternion(0,0,0,0)

	#Now, spawn a ball at each of these coordinates
	for i in range(len(coordSet)):
		
		#Wait for service to respond, ensuring that requests don't get missed
		rospy.wait_for_service("gazebo/spawn_sdf_model")
		
		#Format a spawn request
		request = SpawnModelRequest()
		request.initial_pose.position.x=coordSet[i][0]
		request.initial_pose.position.y=coordSet[i][1]
		request.initial_pose.position.z=0.25
		request.model_xml = ball_xml
		request.model_name = "Ball" + str(i)
		
		#Try to spawn, printing a message if it fails
		if not spawnModelProxy(request):
			print "Unable to spawn model."

	print "Spawned models"	
