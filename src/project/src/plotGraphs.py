#Automated script to plot a graph for each set of test data

import csv
import matplotlib.pyplot as plt

baseFolderPath = "../testing_data"
plotFolderPath = "../testing_data/plots/"
strategyFolders = ["deliberative_search", "random_search", "reactive_search"]
layoutFolders = ["clustered_layout", "grid_layout", "uniform_random_layout"]
strategyStrings = ["deliberative", "random", "reactive"]
layoutStrings = ["Clustered", "Grid", "Uniform"]
turtleBotCounts = [1, 4, 8]

def plotGraph(strategy, layout, turtlebots, lineLabel):	
		#Plots the graph of a given test
		
		if strategy == "deliberative":
			i = 0
		elif strategy == "random":
			i = 1
		elif strategy == "reactive":
			i = 2
		else:
			print "INVALID STRAT"
			exit()
		
		if layout == "clustered":
			j = 0
		elif layout == "grid":
			j = 1
		elif layout == "uniform":
			j = 2
		else:
			print "INVALID STRAT"
			exit()
			
		if turtlebots == 1:
			k = 0
		elif turtlebots == 4:
			k = 1
		elif turtlebots == 8:
			k = 2
		else:
			print "INVALID STRAT"
			exit()
			
		#Generate a string for the directory where the test data is stored
		testDataFolderPath = "{}/{}/{}/{}{}{}Turtlebots".format(baseFolderPath, strategyFolders[i], layoutFolders[j], strategyStrings[i], layoutStrings[j], turtleBotCounts[k])
			
		#How many timesteps each test runs for
		maximumTimeSteps = 300
				
		#Find the index of the total collected targets, since the number of turtlebots in the search affects the number of columns in the csv file
		totalIndex = 26+turtleBotCounts[k]
				
		#Now, for each timestep read in the number of targets collected in each test
		timeSteps = []
		allTotals = []
		
		#Check each file in turn
		for n in range(10):
			collected = []
			testFileName = "test{}.csv".format(n)
			testFilePath = "{}/{}".format(testDataFolderPath, testFileName)
				
			#Account for the fact that fewer than 10 tests were needed for the deliberative grid based search (since it included no element of randomness)
			try:
				with open(testFilePath, "rb") as csvfile:
					reader = csv.reader(csvfile, delimiter="|")
						
					#Read through each row
					rowIndex = 0
					for row in reader:
						#Skip the first 3 rows
						if rowIndex>=3:
							#Make a list of total collected at each timestep
							collected.append(int(row[totalIndex]))
						else:
							rowIndex+=1
			except:
				pass
					
			#Add this to the list we're producing
			allTotals.append(collected)
		
		#Now, calculate averages for each timestep
		averagesAtTimestep = []
		for n in range(maximumTimeSteps):
			timeSteps.append(n+1)
				
			#Make a list of the total collected at this time in each test
			totalsAtTime = []
			for m in range(10):
				#Account for the fact that a few tests were stopped once all targets were found, by catching any indexing errors
				try:
					totalsAtTime.append(allTotals[m][n])
				except:
					pass
					
			#Average this list
			average = sum(totalsAtTime)/float(len(totalsAtTime))
			averagesAtTimestep.append(average)
				
		#Now, for each set of tests - generate a graph showing average collected targets at each timestep
		strategies = ["Deliberative", "Random", "Reactive"]
		plotTitle = "{} search strategy with {} TurtleBots - {} layout performance".format(strategies[i], turtleBotCounts[k], layoutStrings[j])
		plt.rcParams["font.family"] = "Liberation serif"			
		plt.rcParams["axes.labelsize"] = 6
		plt.plot(timeSteps, averagesAtTimestep, label=lineLabel)
		axesMinMax = [0, 300, 0, 25]
		plt.axis(axesMinMax)
		plt.grid(True)
		plt.ylabel("Average Targets Found", fontsize=18)
		plt.xlabel("Timestep", fontsize=18)
		
#Plot a range of graphs
params = {"axes.labelsize":"x-large", "font.family" : "Times New Roman"}
plt.rcParams.update(params)

#Deliberative against random on each of the three layouts
#Grid layout
#1 turtlebot
delib_line = plotGraph("deliberative", "grid", 1, "Deliberative Performance")
random_line = plotGraph("random", "grid", 1, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 1, 4 & 8 TurtleBots - Grid layout"
plotFileName = "DelibVRandomGrid1TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)

#4 turtlebot
delib_line = plotGraph("deliberative", "grid", 4, "Deliberative Performance")
random_line = plotGraph("random", "grid", 4, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 4 TurtleBots - Grid layout"
plotFileName = "DelibVRandomGrid4TurtleBot.png"

#8 turtlebot
delib_line = plotGraph("deliberative", "grid", 8, "Deliberative Performance")
random_line = plotGraph("random", "grid", 8, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 8 TurtleBots - Grid layout"
plotFileName = "DelibVRandomGrid148TurtleBot.png"
plt.savefig(plotFolderPath+plotFileName)
plt.close()	

#Uniform random layout
#1 turtlebot
delib_line = plotGraph("deliberative", "uniform", 1, "Deliberative Performance")
random_line = plotGraph("random", "uniform", 1, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 1, 4 & 8 TurtleBots - Uniform layout"
plotFileName = "DelibVRandomUniform1TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)


#4 turtlebot
delib_line = plotGraph("deliberative", "uniform", 4, "Deliberative Performance")
random_line = plotGraph("random", "uniform", 4, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 4 TurtleBots - Uniform layout"
plotFileName = "DelibVRandomUniform4TurtleBot.png"

#8 turtlebot
delib_line = plotGraph("deliberative", "uniform", 8, "Deliberative Performance")
random_line = plotGraph("random", "uniform", 8, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 8 TurtleBots - Uniform layout"
plotFileName = "DelibVRandomUniform148TurtleBot.png"
plt.savefig(plotFolderPath+plotFileName)
plt.close()

#Clustered layout
#1 turtlebot
delib_line = plotGraph("deliberative", "clustered", 1, "Deliberative Performance")
random_line = plotGraph("random", "clustered", 1, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 1, 4 & 8 TurtleBots - Clustered layout"
plotFileName = "DelibVRandomClustered1TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)

#4 turtlebot
delib_line = plotGraph("deliberative", "clustered", 4, "Deliberative Performance")
random_line = plotGraph("random", "clustered", 4, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 4 TurtleBots - Clustered layout"
plotFileName = "DelibVRandomClustered4TurtleBot.png"


#8 turtlebot
delib_line = plotGraph("deliberative", "clustered", 8, "Deliberative Performance")
random_line = plotGraph("random", "clustered", 8, "Random Performance")
plt.legend(loc="lower right")
plotTitle = "Deliberative and Random strategies with 8 TurtleBots - Clustered layout"
plotFileName = "DelibVRandomClustered148TurtleBot.png"
plt.savefig(plotFolderPath+plotFileName)
plt.close()



#Plot the three reactive searches
#Grid layout
#1 turtlebot
plotGraph("reactive", "clustered", 1, "Deliberative Performance")
plotGraph("reactive", "clustered", 4, "Deliberative Performance")
plotGraph("reactive", "clustered", 8, "Deliberative Performance")
plotTitle = "Reactive search with 1, 4 & 8 TurtleBots - Clustered layout"
plotFileName = "Reactiveclustered.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()

#4 turtlebot
plotGraph("reactive", "uniform", 1, "Deliberative Performance")
plotGraph("reactive", "uniform", 4, "Deliberative Performance")
plotGraph("reactive", "uniform", 8, "Deliberative Performance")
plotTitle = "Reactive search with 1,4 & 8 TurtleBots - Uniform layout"
plotFileName = "ReactiveGrid.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()	

#8 turtlebot
plotGraph("reactive", "grid", 1, "Deliberative Performance")
plotGraph("reactive", "grid", 4, "Deliberative Performance")
plotGraph("reactive", "grid", 8, "Deliberative Performance")
plotTitle = "Reactive search with 1,4 & 8 TurtleBots - Grid layout"
plotFileName = "ReactiveGrid.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()	

#Uniform random layout
#1 turtlebot
delib_line = plotGraph("reactive", "uniform", 1, "Deliberative Performance")
plotTitle = "Reactive search with 1 TurtleBot - Uniform layout"
plotFileName = "ReactiveUniform1TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()

#4 turtlebot
delib_line = plotGraph("reactive", "uniform", 4, "Deliberative Performance")
plotTitle = "Reactive search with 4 TurtleBots - Uniform layout"
plotFileName = "ReactiveUniform4TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()	

#8 turtlebot
delib_line = plotGraph("reactive", "uniform", 8, "Deliberative Performance")
plotTitle = "Reactive search with 8 TurtleBots - Uniform layout"
plotFileName = "ReactiveUniform8TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()

#Clustered layout
#1 turtlebot
delib_line = plotGraph("reactive", "clustered", 1, "Deliberative Performance")
plotTitle = "Reactive search with 1 TurtleBot - Clustered layout"
plotFileName = "ReactiveClustered1TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()

#4 turtlebot
delib_line = plotGraph("reactive", "clustered", 4, "Deliberative Performance")
plotTitle = "Reactive search with 4 TurtleBots - Clustered layout"
plotFileName = "ReactiveClustered4TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()	

#8 turtlebot
delib_line = plotGraph("reactive", "clustered", 8, "Deliberative Performance")
plotTitle = "Reactive search with 8 TurtleBots - Clustered layout"
plotFileName = "ReactiveClustered8TurtleBot.png"
plt.suptitle(plotTitle, fontsize=18)
plt.savefig(plotFolderPath+plotFileName)
plt.close()	
