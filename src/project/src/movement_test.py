#!/usr/bin/env python


import numpy as np
import rospy
import sys
import math 
import tf

from decimal import Decimal
from geometry_msgs.msg import Point, Vector3
from gazebo_msgs.srv import GetModelState

class movementTest():
		
	def __init__(self):
		
		#Begin by initiating the ros node
		rospy.init_node("movement_test_node", anonymous = True)
		
		topicName = "turtlebot0/movement_vector"
		
		self.movementPublisher = rospy.Publisher(topicName, Vector3, queue_size = 5)
		
		self.destinations = [(2,2), (-2,2), (-2,-2), (2,-2)]
		self.destinationIndex = 0
		
		#Create proxy for model state service
		rospy.wait_for_service("gazebo/get_world_properties")
		try:
			self.modelStateProxy = rospy.ServiceProxy("gazebo/get_model_state", GetModelState)
		
		except Exception, e:
			print "Problem creating service proxy", e
			
		#finally, launch the main loop
		self.running = True
		self.startMovementLoop()

	def startMovementLoop(self):
		
		while self.running:
			self.updateDestinations()
			self.publishVectors()
			
	def updateDestinations(self):
		
		tolerance = 0.5		#Distance within which turtlebot must pass from destination to have "reached it", given in metres
		
		destination = self.destinations[self.destinationIndex]
		
		#Get current location from ros service call
		rospy.wait_for_service("gazebo/get_model_state")
		modelState = self.modelStateProxy("Turtlebot0", "world")
		
		distance = math.sqrt( math.pow( destination[0]-modelState.pose.position.x, 2) + math.pow( destination[1]-modelState.pose.position.y, 2))
		
		if distance < tolerance:
			print "Reached destination with distance: ", distance
			if self.destinationIndex == len(self.destinations)-1:
				self.destinationIndex = 0
			else:
				self.destinationIndex += 1
			
	def publishVectors(self):
		#Read current position in the world frame
		currentPosition = self.modelStateProxy("Turtlebot0", "world")
		targetPoint = self.destinations[self.destinationIndex]
		
		#print "currentPos: ", currentPosition
		#print "targetPoint: ", targetPoint
		#Calculate the vector to destination in world frame
		vectorWorldFrame = Vector3()
		vectorWorldFrame.x = targetPoint[0] - currentPosition.pose.position.x
		vectorWorldFrame.y = targetPoint[1] - currentPosition.pose.position.y
			
		#Rotate the vector to convert it into the turtlebot's coordinate system
		# Note, it is necessary to calculate rotation from the quaternion system used by ROS, hence the long formula
		quaternion = (currentPosition.pose.orientation.x, currentPosition.pose.orientation.y, currentPosition.pose.orientation.z, currentPosition.pose.orientation.w)
		rollPitchYaw = tf.transformations.euler_from_quaternion(quaternion)
			
		rotation = rollPitchYaw[2]
			
		vectorTurtlebotFrame = Vector3()
		vectorTurtlebotFrame.x = (vectorWorldFrame.x*np.cos(-rotation) - vectorWorldFrame.y*np.sin(-rotation))
		vectorTurtlebotFrame.y = (vectorWorldFrame.x*np.sin(-rotation) + vectorWorldFrame.y*np.cos(-rotation))
		
		#Publish the resulting vector and optionally, the point in world coordinates that it is headed for
		#print "TBot frame: ", vectorTurtlebotFrame
		#print "Rotation: ", rotation
		self.movementPublisher.publish(vectorTurtlebotFrame)
		
def main(args):
	#Initiate the node, handing in the number of turtlebots as an argument
	test = movementTest()
	

if __name__=="__main__":
	main(sys.argv)
