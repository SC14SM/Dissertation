#!/usr/bin/env python

import rospy
import cv2
import numpy as np
import sys
import math

from std_msgs.msg import Bool, Float64
from geometry_msgs.msg import Twist, Vector3


#	Class to control a single turtlebot during a random movement search. Takes the outputs of various nodes and forms a single consistent set of behaviours.
# 	Follows a combination of a random direction vector, along with an avoidance vector that prevents the turtlebot moving into obstacles.
# 	When a target is spotted within some distance, it then goes directly to the target, removes it and returns to its previous behaviour

#	Presumes the existence of several other nodes:
#	-Some sort of detector node publishing to target_vector
#	-Some sort of avoidance node publishing to laser_avoidance
#	-Some sort of grabber node listening on grabber_trigger and publishing to grabber_return
#	-Some sort of movement node publishing on smooth_movement_commands and listening on movement_vector
#	-Some sort of guidance node publishing on guidance_vector

#	This class is not concerned with how these nodes are actually implemented, for instance any one of several movement types can easily be swapped in or out without affecting the 
# 	operation of this node. Likewise, although this node was written with a simplified version of the target detection in place, a more sophisticated detector node can easily
#	be swapped in during the launch process without any adjustments needed to this code.

class controlNode():
	
	def __init__(self, pickupDistance = 0.5):
		
		#Start by initiating the ros node
		rospy.init_node("Control_node", anonymous = True)
				
		#Some variables determining how close a piece of rubbish has to be for the turtlebot to leave its route and pick it up
		#These can be adjusted via another topic
		self.detectionThreshold = 2.0	
		self.pickupThreshold = pickupDistance

		#Create a timer, specifying how often to process all the vectors - our system doesn't move fast enough to need more than a few per second
		maxUpdateFrequency = 5
		self.updateTimer = rospy.Rate(maxUpdateFrequency)
		
		#Initialise variables for the subscribers
		self.latestDetectorVector = Vector3()
		self.latestAvoidanceVector = Vector3()
		self.latestCentroidVector = Vector3()
		self.latestRandomVector = Vector3()
		self.latestMovementMessage = Twist()
		self.latestGradient = Float64()
		self.grabberDone = False

		#Create subscribers to these nodes we've just created
		detectorSubscriber = rospy.Subscriber("target_vector", Vector3, self.detectorCallback)
		avoidanceSubscriber = rospy.Subscriber("laser_avoidance_vector", Vector3, self.avoidanceCallback)
		randomSubscriber = rospy.Subscriber("random_vector", Vector3, self.randomCallback)
		centroidSubscriber = rospy.Subscriber("centroid_vector", Vector3, self.centroidCallback)
		gradientSubscriber = rospy.Subscriber("intensity_gradient", Float64, self.intensityCallback)
		grabberSubscriber = rospy.Subscriber("grabber_return", Bool, self.grabberCallback)
		movementSubscriber = rospy.Subscriber("movement_commands", Twist, self.movementCallback)
		
		#Create a publisher to communicate with the base and grabber arm
		self.movementPublisher = rospy.Publisher("movement_vector", Vector3, queue_size=5)
		self.basePublisher = rospy.Publisher("mobile_base/commands/velocity", Twist, queue_size=5)
		self.grabberPublisher = rospy.Publisher("grabber_trigger", Vector3, queue_size=5)
		
		#Finally, start the main loop
		self.startControlLoop()
	
	def startControlLoop(self):
		#Handle the main bulk of the control logic
		
		while not rospy.is_shutdown():
			
			#Calculate magnitude of detection vector, storing it here so it doesn't change while we are using it
			detectorVector = self.latestDetectorVector
			targetDistance = math.sqrt(math.pow(detectorVector.x, 2) + math.pow(detectorVector.y,2))
			
			#Check if dectection is within threshold - if it equals zero then nothing has been detected so we go to the other clause
			if (targetDistance!=0.0) and (targetDistance <= self.detectionThreshold):
				
				if float(targetDistance) < self.pickupThreshold:
					print targetDistance, self.pickupThreshold
					self.pickup(detectorVector)
				else:
					#Move towards the target, taking the avoidance vector into account
					vectorSum = Vector3((detectorVector.x+self.latestAvoidanceVector.x), (detectorVector.y+self.latestAvoidanceVector.y), 0)
					#Publish this vector to the movement node to be converted into actual movement commands
					self.movementPublisher.publish(vectorSum)
			else:
				#Use the gradient of the summed intensity from the target detection image to determine the weighting between random movement and moving towards target
				
				#If the gradient is increasing, then we don't make any random turns
				grad = self.latestGradient.data
				if grad >= 0:
					randomWeighting = 0
				else:
					#Otherwise, we use a function that ramps up quickly and equals 1 at -1 (the minimum value published by the gradient node)
					randomWeighting = 1-math.pow((-grad-1), 4)
				
				#Calculate a weighted sum of the two vectors, including our avoidance vector
				vectorSum = Vector3()
				vectorSum.x = self.latestRandomVector.x*randomWeighting + self.latestCentroidVector.x*(1-randomWeighting) + self.latestAvoidanceVector.x
				vectorSum.y = self.latestRandomVector.y*randomWeighting + self.latestCentroidVector.y*(1-randomWeighting) + self.latestAvoidanceVector.y
					
				#Publish this to the movement node to be converted into movement commands	
				self.movementPublisher.publish(vectorSum)
			
			#Now publish the actual movement commands generated from the movement node to the base, 
			self.basePublisher.publish(self.latestMovementMessage)
			
			#Sleep, so that we don't waste resources updating more frequently than we need to
			self.updateTimer.sleep()
	
	def intensityCallback(self, message):
		#Store the latest gradient for use by the control loop
		self.latestGradient = message
	
	def detectorCallback(self, message):
		#Store the latest vector for use by the control loop
		self.latestDetectorVector = message
		
	def avoidanceCallback(self, message):
		#Store the latest vector for use by the control loop
		self.latestAvoidanceVector = message

	def centroidCallback(self, message):
		#Store the latest vector for use by the control loop
		self.latestCentroidVector = message

	def grabberCallback(self, message):
		#Set the flag for grabber finishing its routine
		self.grabberDone = True
	
	def movementCallback(self, message):
		#Store the latest twist message for use by the control loop
		self.latestMovementMessage = message
		
	def randomCallback(self, message):
		#Store the latest random vector
		self.latestRandomVector = message

	def pickup(self, vector):
		#Causes the turtlebot to remove a piece of rubbish in a small area in front of it
		
		#Publish an empty movement vector to stop the base
		self.basePublisher.publish(Twist())
		print "Picking up target..."
		#Wait for a short period to mimic the time taken for a grabber arm to remove the target
		rospy.sleep(1)
		
		#Call the grabber arm to remove the object
		self.grabberPublisher.publish(vector)
		
		#Publish a blank detection vector so that the system knows the target in front of it is no longer there
		self.latestDetectorVector = Vector3()
		
def main(args):
	control = controlNode(args[1])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)
