#!/usr/bin/env python

# 	Takes a downsampled version of the depth camera feed, and detects objects within it by clustering connected groups of pixels together.
#  	Connected groups of pixels are clustered together as long as there is a smooth transition of depth value from one pixel to the next,
#	with adjacent clusters merging if they are connected by two pixels with similar depth values.

# 	Once the pixels in the depth image have been separated into clusters, this node then calculates a bounding square containing each detected 
#	cluster and publishes these squares to a dedicated topic. These published bounding squares are used to identify regions of the image to feed
#	into the neural classifier.

#	This node forms a key part of the improved vision system.

import rospy
import cv2
import numpy as np
import sys

from project.msg import bounding_squares
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class DepthImageClusterer():
	
	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("Depth_Image_Clusterer", anonymous = True)
		
		#Instantiate the cv bridge to convert the image for openCV
		self.bridge = CvBridge()
		
		#Create a publisher for the clustered image and the bounding squares
		self.clusteredImagePublisher = rospy.Publisher("clusteredImage", Image, queue_size = 3)
		self.squaresPublisher = rospy.Publisher("boundingSquares", bounding_squares, queue_size =3)
		
		#Create a timer, so that images are only published with some maximum frequency
		maxFrequency = 4
		publishTimer = rospy.Rate(maxFrequency)
		
		#Create a subscriber to the depth image topic
		depthSubscriber = rospy.Subscriber("convertedDepth", Image, self.imageCallback)
		
		self.receivedImage = False

		#Wait for an image to be received:
		while (not rospy.is_shutdown()) and (not self.receivedImage):
			publishTimer.sleep()
			
		#Start the main publishing loop
		while not rospy.is_shutdown():
			self.findClusters()
			self.findBoundingSquares()
			self.publishBoundingSquares()
			publishTimer.sleep()
			
	def imageCallback(self, image):
		
		#Simply store the image, so that the callback function doesn't get bogged down and fall behind
		self.latestImage = image
		#At some point earlier on I got height and width switched around, so these next two lines are as intended even if it seems odd
		self.imageWidth = image.height
		self.imageHeight = image.width
		self.receivedImage = True

		
	def findClusters(self):
		#Takes the latest received image and finds clusters in it
		
		#Keeps track of which clusters have been found, since some might be removed through merging
		self.clusters = []
		self.nextClusterIndex = 1
			
		#Start by converting to cv2 format
		self.image = self.bridge.imgmsg_to_cv2(self.latestImage)
			
		#Now, convert image to signed int so we don't get weird rollover errors
		self.image = self.image.astype(np.int16)
			
		#Generate an array to keep track of which pixel is assigned to which cluster
		self.assignments = np.zeros((self.imageWidth, self.imageHeight), dtype = np.uint8)
			
		#Go through all pixels in the image
		for i in range(self.imageWidth):
			for j in range(self.imageHeight):
				#When we find a non black pixel we check to see if it should be in the same cluster as either of its neighbours above and to the left
				if not self.image[i][j] == 0: 
					self.checkNeighbours(i, j) 
						
		#Print the number of clusters found:
		self.clusteredImagePublisher.publish(self.bridge.cv2_to_imgmsg(self.assignments))
						
	def checkNeighbours(self, i, j):
		#Checks a pixels neighbours to see if they are close enough in value to be assigned to the same cluster as it, then calls this
		# function on them if so
		
		valueThreshold = 10
		
		matchesHorizontalNeighbour = False
		matchesVerticalNeighbour = False
		horizontalNeighbourCluster = 0
		verticalNeighbourCluster = 0
		
		#Check neighbour to the left
		#Check it's a valid index
		if i-1 >= 0:
			#Check not black pixel
			if self.image[i-1][j] != 0:
				#Check if it's in the threshold for being assigned to the same cluster
				if (self.image[i][j] > (self.image[i-1][j]-valueThreshold)) and (self.image[i][j] < (self.image[i-1][j]+valueThreshold)):
					#If this pixel isn't already assigned add it to the current cluster
					matchesHorizontalNeighbour = True
					horizontalNeighbourCluster = self.assignments[i-1][j]
						
		#Check neighbour above
		#Check it's a valid index
		if j-1 >= 0:
			#Check not a black pixel
			if self.image[i][j-1] != 0:
				#Check if it's in the threshold for being assigned to the same cluster
				if (self.image[i][j] > (self.image[i][j-1]-valueThreshold)) and (self.image[i][j] < (self.image[i][j-1]+valueThreshold)):
					matchesVerticalNeighbour = True
					verticalNeighbourCluster = self.assignments[i][j-1]
					
		#Now assign the pixel to a cluster based on its neighbours values, potentially merging two clusters 
		if matchesHorizontalNeighbour:
			#If both match, we merge clusters if they aren't already in the same cluster
			if matchesVerticalNeighbour:
				self.assignments[i][j] = horizontalNeighbourCluster
				if horizontalNeighbourCluster != verticalNeighbourCluster:
					self.mergeClusters(horizontalNeighbourCluster, verticalNeighbourCluster)
				
			#If horizontal matches but not vertical we take that value
			else:
				self.assignments[i][j] = horizontalNeighbourCluster
				
		else:
			#and vice versa
			if matchesVerticalNeighbour:
				self.assignments[i][j] = verticalNeighbourCluster
			
			#If pixel matches neither neighbour then we add it to a new cluster
			else:
				self.assignments[i][j] = self.nextClusterIndex
				self.clusters.append(self.nextClusterIndex)
				self.nextClusterIndex += 1
				
	
	def mergeClusters(self, indexA, indexB):
		#Shifts the value of every pixel assigned to indexB to be assigned to indexA instead
		for i in range(self.imageWidth):
			for j in range(self.imageHeight):
				if self.assignments[i][j] == indexB:
					self.assignments[i][j] = indexA
		#Remove cluster B from the list of clusters
		self.clusters.remove(indexB)
		
	def findBoundingSquares(self):
		#Finds the bounding squares for each detected cluster
		
		leftMostPixelIndexes = [9999]*len(self.clusters)
		rightMostPixelIndexes = [0]*len(self.clusters)
		upperMostPixelIndexes = [9999]*len(self.clusters)
		bottomMostPixelIndexes = [0]*len(self.clusters)
		
		#Scout through each pixel in the clustered image, finding the leftmost, rightmost pixels in each cluster etc.
		for i in range(self.imageWidth):
			for j in range(self.imageHeight):
				pixelCluster = self.assignments[i][j]
				
				#Check it's assigned to some cluster
				if pixelCluster != 0:
					#Get the index of this cluster in the clusters list, since the list can contain non contiguous values,
					# for instance [1,2,4,6] because of merging clusters
					clusterIndex = self.clusters.index(pixelCluster)
					
					#Compare each pixel with the current bounding values and update them
					if leftMostPixelIndexes[clusterIndex] > i:
						leftMostPixelIndexes[clusterIndex] = i
					
					if rightMostPixelIndexes[clusterIndex] < i:
						rightMostPixelIndexes[clusterIndex] = i
					
					if upperMostPixelIndexes[clusterIndex] > j:
						upperMostPixelIndexes[clusterIndex] = j
					
					if bottomMostPixelIndexes[clusterIndex] < j:
						bottomMostPixelIndexes[clusterIndex] = j
					
		#Now that we've found the bounding pixels of each cluster, we form a square by calculating the topleft pixel of said square and the length of each side
		self.topIndexes = [0]*len(self.clusters)
		self.leftIndexes = [0]*len(self.clusters)
		self.sideLengths = [0]*len(self.clusters)
		
		#For each cluster:
		for i in range(len(self.clusters)):
			#Check if its width or height are greater
			width = rightMostPixelIndexes[i] - leftMostPixelIndexes[i] + 1
			height = bottomMostPixelIndexes[i] - upperMostPixelIndexes[i] + 1
			
			#Center the image
			
			if width > height:
				self.sideLengths[i] = width
				#Adjust the startPixelIndex so that the image is centred
				extraVerticalSpace = (self.sideLengths[i] - height)
				if upperMostPixelIndexes[i] - (extraVerticalSpace/2) > 0:
					upperMostPixelIndexes[i] -= extraVerticalSpace/2
				else:
					upperMostPixelIndexes[i] = 0
			else:
				self.sideLengths[i] = height
				#Adjust the startPixelIndex so that the image is centred
				extraHorizontalSpace = (self.sideLengths[i] - width)
				if leftMostPixelIndexes[i] - (extraHorizontalSpace/2) > 0:
					leftMostPixelIndexes[i] -= extraHorizontalSpace/2
				else:
					leftMostPixelIndexes[i] = 0
				
			
			#Store the topleft corner as a tuple
			self.topIndexes[i] = upperMostPixelIndexes[i]
			self.leftIndexes[i] = leftMostPixelIndexes[i]
			
	def publishBoundingSquares(self):
		#Publishes the bounding squares for each cluster to a dedicated topic for this info	
		
		#Form an instance of a custom message type that we defined elsewhere in the project
		outputMessage = bounding_squares()
		outputMessage.count = len(self.clusters)
		outputMessage.top_pixel_indexes = self.topIndexes
		outputMessage.left_pixel_indexes = self.leftIndexes
		outputMessage.side_lengths = self.sideLengths
		
		#Publish this message
		self.squaresPublisher.publish(outputMessage)
				
		
def main(args):
	#if this is main path, launch an instance
	clusterer = DepthImageClusterer()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)
