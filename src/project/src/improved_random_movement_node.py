#!/usr/bin/env python

#	Improvement over random_movement_node, previous node simply published a random direction vector (from the robots perspective) at all times, but as
#	the robot turned, the vector turned with it so it didn't really model brownian motion at all
#	Crucially, this version doesn't have any chance to turn until it has already finished its previous turn, and in between turns it moves 
#	directly forward. This gives a much better approximation of brownian motion than the previous implementation.

#	Generates random movement for use in the random search as well as the reactive search.

import numpy as np
import rospy
import sys
import math 
import random

from geometry_msgs.msg import Twist, Vector3

class randomMovementNode():
	
	def __init__(self, namespaceIndex, turnProb, frequency):
		#Initiate ros node
		rospy.init_node("random_movement_vector", anonymous=True)
		
		#Define the angular speed we use in the movement node - this shouldn't change since it is dependent on the hardware manufacturers guidelines
		self.angularSpeed = math.pi/6
		
		#Read in constructor arguments
		self.perTimestepTurnChance = float(turnProb)
		self.topicName="random_vector"
		
		#Instantiate the required objects
		self.timer = rospy.Rate(10)
		self.turnTimer = rospy.Rate(10)
		self.publisher = rospy.Publisher(self.topicName, Vector3, queue_size=5)
		self.turning = False
		
		#Begin with some random turn
		self.randomTurn()
		
		while not rospy.is_shutdown():
			#Repeatedly call a function to change the vector direction with some probability
			#Function includes a sleep call so that it gets called at most with the given frequency
			self.updateVector()
			self.timer.sleep()
	
	def randomTurn(self):
		#Generate and publish a random turn, sleeping until the turn is complete
		randomRotation = (random.random()*math.pi*2) - math.pi	#Generate an angle in the range [-pi..pi]
		
		#Determine how long we have to sleep for to make a turn of this magnitude
		turnTime = abs(randomRotation)/self.angularSpeed		
		
		#Publish a vector, depending on whether we want to turn left or right
		if randomRotation > 0:
			outputVector = Vector3(0,-1,0)
		else:
			outputVector = Vector3(0,1,0)
		
		#Set a flag, and start the turn - sleeping to give it time to complete
		self.turning = True
		self.publisher.publish(outputVector)
		
		#Sleep while we complete the turn
		rospy.sleep(turnTime)
		
		#Reset the flag and publish an empty vector to finish the turn
		self.turning = False
		self.publisher.publish(Vector3())
	
	def updateVector(self):
		#Generate a random number and compare to the probability given to the constructor
		generated = random.random()
		
		if not self.turning:	#Check we aren't mid turn
			#With some probability make a random turn
			if generated <= self.perTimestepTurnChance:
				self.randomTurn()
			#Otherwise, just carry on forward
			else:
				self.publisher.publish(Vector3(1,0,0))
			

def main(args):
	randomNode = randomMovementNode(args[1], args[2], args[3])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)

