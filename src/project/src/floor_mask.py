#!/usr/bin/env python

import rospy
import cv2
import numpy as np
import sys
import struct

from sensor_msgs.msg import PointCloud2, PointField, Image
from cv_bridge import CvBridge, CvBridgeError

#	Node which looks at the point cloud data produced by the kinect sensor, and uses it to produce an image mask - telling us which pixels are part of the floor.
#	with "part of the floor" determined by the y coordinate of each point.
#	For each pixel in the image, if the corresponding point cloud entry is within some tolerance of the floor (calculated from the known height of the turtlebot camera)
#	it is given a value of 0 in the output, otherwise the value is 255.

# 	Used as an initial step in object detection

class floor_mask_node():
	
	def __init__(self):
		
		#Begin by initiating a ros node
		rospy.init_node("Floor_Mask_Node", anonymous = True)
		
		#Subscribe to the point cloud feed
		pointSubscriber = rospy.Subscriber("camera/depth/points", PointCloud2, self.pointCloudCallback)
		self.pointsPublished = False
		
		#Publisher to handle output
		self.maskPublisher = rospy.Publisher("floorMask", Image, queue_size=5)
		
		#CV bridge to make a ros image message from the array of ints
		self.bridge = CvBridge()
		
		#Repeatedly publish points with some max frequency
		maxPublishFrequency = 5
		publishTimer = rospy.Rate(maxPublishFrequency)
		while not rospy.is_shutdown():
			self.publishFloorMask()
			publishTimer.sleep()
			

	def pointCloudCallback(self, points):
		#Store the set of points for use by the publish function
		self.latestPoints = points
		self.pointsPublished = True
		
	def publishFloorMask(self):
		#Generates and publishes a floor mask image
		
		#Check at least one set of pointcloud data has been published 
		if self.pointsPublished:
			#Store the latest set of points locally so they don't change while we are using them
			points = self.latestPoints
			
			#Define a scale factor by which to shrink the image to improve performance
			scaleFactor = 0.25
			
			#Define an array the size of the image, with all values set to 255 (in the mask)
			outputMask = np.full(shape=(int(points.height*scaleFactor), int(points.width*scaleFactor)),fill_value = 255,dtype=np.int8)
			
			#Read through the points in order
			for y in range(int(points.height*scaleFactor)/2, int(points.height*scaleFactor)):	#For this we look only at the bottom half of the image since the floor will never be above the horizon line
				for x in range(int(points.width*scaleFactor)):
					start = ((x*points.point_step) + (y*points.row_step))/scaleFactor
					xOffset = 0
					yOffset = 4		#Offset needed to get the y value for this point, the only value we are concerned with 
					zOffset = 8
					
					#Get the float values from the byte array
					try:
						#xValue = struct.unpack_from("f", points.data, start+xOffset)[0]
						yValue = struct.unpack_from("f", points.data, int(start+yOffset))[0]	#Z value is published as a 32 bit float, so we want 4 bytes from data[]
						#zValue = struct.unpack_from("f", points.data, start+zOffset)[0]
					except Exception, e:
						print e
					
					#If y value matches that of the floor, remove that pixel from the mask with a value of 0
					yDistanceToFloor = 0.3				#Distance from the kinect sensor to the floor (30cm)
					distanceMargin = 0.01				#Acceptable margin for bumps etc, to be considered part of the floor rather than obstacles
					if yValue > (yDistanceToFloor-distanceMargin):
						outputMask[y][x] = 0
						
			#Having produced an array, we convert this to an image message and publish it
			imageMessage = self.bridge.cv2_to_imgmsg(outputMask)
			
			self.maskPublisher.publish(imageMessage)
		
def main(args):
	#if this is main path, launch an instance
	masker = floor_mask_node()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)	
