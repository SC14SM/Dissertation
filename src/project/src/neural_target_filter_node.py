#!/usr/bin/env python

# Node which searches the camera feed for targets, and reports the likelihood that any given region of the feed contains one
# Output is a single channel image feed, in which every pixel's intensity is equal to the likelihood that that pixel contains a target
# Uses a hough transform to identify regions which may contain objects (through number of edge pixels present) then uses the neural network
# to check if these objects are the type we are looking for
#
# The key steps in this node's functioning are as follows:
#
#	1. Perform canny edge detection on a grayscale, downsampled version of the image feed
#	2. Produce a parameter space, describing various sizes of squares centred at different coordinates in the image
#	3. Go through the output of the edge detection, and have each edge pixel "vote" on points in the parameter space
#	4. From step 3, we now know the number of edge pixels that would be contained in a square of a given size at a given location
#	5. Select up to a maximum quantity of these squares (based on concentration of edge pixels) and feed them into the neural classifier
#	6. Produce an output image, with the value of each region equal to neural classifier's calculated probability that that region contains a target
#	7. Publish this output image

import cv2
import numpy as np
import rospy
import sys
import math

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from keras.models import load_model
from keras.models import Sequential
from keras.models import model_from_json
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers import Dense

class targetFilter():
	
	bridge = None					#Converts raw image to cv2 format
	filteredImagePublisher = None 	#ROS publisher for the output image
	
	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("Green_Filter", anonymous=True)
		
		#Set a maximum rate to publish images, so we don't hog too much computation
		#This doesn't need to be too high, since the TurtleBot isn't moving particularly fast
		maxUpdateFrequency = 4
		updateTimer = rospy.Rate(maxUpdateFrequency)
		
		#Define a downsampling factor, to improve performance since we only care about the general outlines of shapes at this stage
		self.downsamplingFactor = 0.25
		self.edgePixelConcentrationThreshold = 0
		self.maxRegionsChecked = [5, 5, 5, 5]		#Number of regions of each size to check
		self.squareSizes =  [8, 16, 24, 32]		#Define the possible side lengths for the squares we'll be placing over the image

		#Instantiate the cv bridge, our subscriber and our publisher
		self.bridge = CvBridge()
		
		#Load in the neural network, which we trained previously from the positive and negative imagesets
		#Begin by loading the structure from a JSON file
		modelJSONFile = open("neuralNetworkStructure.json", "r")
		neuralNetworkJSON = modelJSONFile.read()
		modelJSONFile.close()
		self.neuralNetwork = model_from_json(neuralNetworkJSON)
		
		#Next, load the weights into this structure
		self.neuralNetwork.load_weights("neuralNetworkWeights.h5")
		
		#Read in the raw image data from the kinect and make a variable to keep track of if anything's been published yet
		imageSubscriber = rospy.Subscriber("camera/rgb/image_raw", Image, self.imageCallback)
		self.imagePublished = False
		
		#Publisher outputs the image, once it has been filtered such that only regions of interest remain
		self.filteredImagePublisher = rospy.Publisher("targetMaskedImage", Image, queue_size=1)
		self.edgeImagePublisher = rospy.Publisher("edgeImage", Image, queue_size=1)

		#Start a loop to repeatedly publish images
		while not rospy.is_shutdown():
			self.publishTargetImage()
			updateTimer.sleep()
		
	def imageCallback(self, Image):
		#Called on the image data every time the subscriber reads in new data
		#Simply stores for use by the other function		
		self.latestImage = Image
		self.imagePublished = True
			
	def publishTargetImage(self):
		

		#First, check that an image has been published
		if self.imagePublished:
			
			#Begin by converting our image to a usable format, this also prevents it changing while we are using it
			try:
				convertedImage = self.bridge.imgmsg_to_cv2(self.latestImage)
				#Produce a downsampled version of the image for performance and convert it to grayscale so that it can be fed into the edge detector
				downsampledImage = cv2.resize(convertedImage, (0,0), fx=self.downsamplingFactor, fy=self.downsamplingFactor, interpolation=cv2.INTER_NEAREST)
				downsampledImage = cv2.cvtColor(downsampledImage, cv2.COLOR_BGR2GRAY)
			except:
				print "Target filter node unable to convert image message to usable format"
				
			#Now, create  a blank image for output
			outputImageXDimension = int(convertedImage.shape[0]*self.downsamplingFactor)
			outputImageYDimension = int(convertedImage.shape[1]*self.downsamplingFactor)
			self.outputImage = np.zeros((outputImageXDimension, outputImageYDimension), dtype="uint8")
					
			#Generate thresholds for edge detection based on otsu's method
			upperThreshold, thresh = cv2.threshold(downsampledImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
			lowerThreshold = 0.5*upperThreshold
			
			#Perform edge detection on the resulting image, using canny edge detector
			kernelSize = 3	#Use default kernel size
			edgeImage = cv2.Canny(downsampledImage, lowerThreshold, upperThreshold)
			
			#Now, remove the line of edge pixels that make up the horizon in the image
			horizonLineStart = 66
			horizonLineEnd   = 68
			edgeImage[horizonLineStart:horizonLineEnd][0:] = 0
			
			#Publish the output of the edge detection process
			self.edgeImagePublisher.publish(self.bridge.cv2_to_imgmsg(edgeImage))
			
			#Now for the next step we perform a hough transform to identify squares in the image with high concentrations of edge pixels
			
			#begin by defining a data structure for the parameter space of the hough transform
			#discretisation factor defines how much smaller the parameter space is than the image itself
			discretisationFactor = 0.2
			parameterSpaceX = int(edgeImage.shape[0]*discretisationFactor)
			parameterSpaceY = int(edgeImage.shape[1]*discretisationFactor)
			parameterSpaceZ = len(self.squareSizes)
			accumulator = np.zeros((parameterSpaceX, parameterSpaceY, parameterSpaceZ))
			
			#Now, go through every pixel in the edge image, excluding the bottom 20% of the image and the top half - since these areas never contain targets
			for i in range(int(edgeImage.shape[0]*0.5), int(edgeImage.shape[0]*(0.8))):
				for j in range(edgeImage.shape[1]):
					
					#Check if the pixel is non-zero, meaning it is part of an edge
					if edgeImage[i][j] != 0:
						
						#Vote for every point in the parameter space which describes a square that contains this pixel
						#Check for each of the possible square sizes in turn
						for k in range(parameterSpaceZ):
							
							sideLength = self.squareSizes[k]
							
							#Calculate the set of points for which, a square of this size with its top left corner at that point would contain this coordinate
							votingRegionLeftBoundary = j-sideLength
							votingRegionTopBoundary = i-sideLength
							
							#Avoid array index errors, by never letting the top or left indexes be below zero
							if votingRegionLeftBoundary < 0:
								votingRegionLeftBoundary = 0
							if votingRegionTopBoundary < 0:
								votingRegionTopBoundary = 0
								
							#Define the right and bottom boundary of the voting region at this coordinate, since no square with its top left corner below 
							# or to the right of this coordinate will contain it.
							votingRegionRightBoundary = j
							votingRegionBottomBoundary = i
							
							#Now, calculate the area within the parameter space to vote for, since it is smaller than the image itself
							parameterSpaceLeftBoundary = int(math.floor(votingRegionLeftBoundary*discretisationFactor))
							parameterSpaceRightBoundary = int(math.ceil(votingRegionLeftBoundary*discretisationFactor))
							parameterSpaceTopBoundary = int(math.floor(votingRegionTopBoundary*discretisationFactor))
							parameterSpaceBottomBoundary = int(math.floor(votingRegionBottomBoundary*discretisationFactor))
							
							#For each point within this region in the parameter space, assign one vote
							accumulator[parameterSpaceTopBoundary:parameterSpaceBottomBoundary, parameterSpaceLeftBoundary:parameterSpaceRightBoundary, k] += 1
										
			#Calculate the areas of each size of square to save us repeatedly needing to do so
			squareAreas = []
			for i in range(len(self.squareSizes)):
				squareAreas.append(self.squareSizes[i]**2)
				
			#From the parameter space, see which sets of parameters give us regions with the highest concentration of edge pixels - these are the regions we will check with the neural classifier
			
			#Keep track of interesting regions here, seperated by size since we want a few of each size
			regionsToCheck = []
			for i in range(parameterSpaceZ):
				regionsToCheck.append([])
			
			#Now, look at each possible x,y position to place a square in the parameter space 
			for x in range(parameterSpaceX):
				for y in range(parameterSpaceY):
						
					for z in range(parameterSpaceZ):
						
						#Calculate this accumulator's activation over the size of the area it relates to
						activation = accumulator[x][y][z]
						activationOverArea = float(activation)/squareAreas[z]
						
						#If this region contains some edge pixels then we add it to the list of potential regions to check
						if activation > 0:
							
							#Calculate the pixel coordinates of this x,y in the downsampled image
							imageXCoord = int(x/discretisationFactor)
							imageYCoord = int(y/discretisationFactor)
							
							#Add this square to the list of image regions to check
							regionsToCheck[z].append((imageXCoord, imageYCoord, self.squareSizes[z], activationOverArea))
							
			
			#Now, select a few regions of each size here, so that regions of a given size are non-overlapping, with a set number selected of each size
			for i in range(parameterSpaceZ):	
				#Sort according to their activation over area, since these are the ones we want to sample first
				regionsToCheck[i].sort(key=lambda x: x[3], reverse=True)
								
				#Keep track of how many we've selected
				checkedCount = 0
				previouslyCheckedRegions = []
							
				#Look at each region in the list
				for region in regionsToCheck[i]:
					
					#Make sure we haven't already checked the maximum number of regions
					if checkedCount < self.maxRegionsChecked[i]:
						
						#Check this region doesn't overlap with any previously checked regions of the same size
						overlapping = False
						for previous in previouslyCheckedRegions:
							if self.regionsOverlap(previous, region):
								overlapping = True
						
						#If it doesn't overlap, then we feed this region to the neural classifier and increment the count of checked regions
						if not overlapping:
							checkedCount += 1
							previouslyCheckedRegions.append((region[0], region[1], region[2]))		#Store a tuple describing the region for comparison later (to avoid overlapping)
							self.calculateTargetProbability(region[0], region[1], region[2])
				
			#Finally, having filled the output image with values we publish it
			self.filteredImagePublisher.publish(self.bridge.cv2_to_imgmsg(self.outputImage))
	
	def regionsOverlap(self, a, b):
		#Takes two square regions defined by the coordinates of the top left pixel and side length,
		# then returns true or false for if those regions overlap
			
		xOverlapping, yOverlapping = False, False
		
		#Calculate the left right top and bottom sides of each square
		aL, bL = a[0], b[0]
		aR, bR = a[0]+a[2], b[0]+b[2]
		aB, bB = a[1], b[1]
		aT, bT = a[1]+a[2], a[1]+a[2]
		
		#Check if a is either to the left or the right of B
		if not ((aL > bR) or (aR < bL)):
			xOverlapping = True
		
		#Now check if a is either below or above B
		if not ((aT < bB) or (aB > bT)):
			yOverlapping = True
		
		#If the squares overlap in the x and y axis then they overlap spatially
		if xOverlapping and yOverlapping:
			return True
		else:
			return False
	
	def calculateTargetProbability(self, x, y, sideLength):
		#Crops out a square with a given sidelength and top left corner (x,y), from the image feed then feeds it into the neural classifier
		#.. once the output is found, it updates the pixel values of the output image within this square to reflect how likely
		#.. the classifier thinks the region is to contain a target.
		
		#Begin by converting the image feed into a usable format
		try:
			convertedImage = self.bridge.imgmsg_to_cv2(self.latestImage)
		except:
			print "Target filter node unable to convert image message to usable format"
		
		#Calculate the coordinates of the region we are interested in IN THE NON-DOWNSAMPLED IMAGE
		scaledX = int(x/self.downsamplingFactor)
		scaledY = int(y/self.downsamplingFactor)
		scaledSideLength = int(sideLength/self.downsamplingFactor)
		
		#Now, crop out the region we are interested in
		croppedArea = convertedImage[scaledX:scaledX+scaledSideLength, scaledY:scaledY+scaledSideLength]
		
		#Resize the image we just cropped out to the desired size for the classifier
		desiredSize = (32,32)
		resizedImage = cv2.resize(croppedArea, desiredSize, interpolation=cv2.INTER_LINEAR)
		
		#Feed the image into the classifier, after placing it into an np array rescaling pixel values to [0..1] from [0..255]
		inputArray = np.zeros((1, 32, 32, 3))
		inputArray[0] = resizedImage
		inputArray = np.array(inputArray, dtype="float")/255.0
		
		#In the output array from prediction, first value is probability non-target and second value is probability target
		neuralOutput = self.neuralNetwork.predict(inputArray)
		targetProbability = neuralOutput[0][1]
		
		#Finally, adjust the brightness of the relevant region of the output image to reflect how likely it is to contain a target
		maxBrightness = 255
		#regionBrightness = 30
		regionBrightness = maxBrightness*targetProbability

		#For each pixel in the relevant region of the output image update its brightness
		
		#Avoid array indexing errors here
		if x+sideLength < self.outputImage.shape[0]:
			regionWidth = sideLength
		else:
			regionWidth = self.outputImage.shape[0]-(x+1)
		
		if y+sideLength < self.outputImage.shape[1]:
			regionLength = sideLength
		else:
			regionLength = self.outputImage.shape[1]-(y+1)
			
			
		for i in range(regionWidth):
			for j in range(regionLength):
				#Only update the brightness of a pixel if this would increase it
				if (self.outputImage[x+i][y+j] + regionBrightness) > 255:
					self.outputImage[x+i][y+j] = 255
				else:
					self.outputImage[x+i][y+j] += regionBrightness
def main(args):
	#If this is the main path, launch an instance
	node = targetFilter()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)		
		
		
