#!/usr/bin/env python

#	Automatically runs a number of tests, accepting various parameters, number of tests and type of test etc.
#	then logs the results to a series of .csv files
#	used to produce graphs and figures for the final report.
#
#	Outputs to a .csv file for ease of reference and further analysis

import numpy as np
import rospy
import sys
import math 
import os
import csv
import tf
import ball_spawner_grid
import ball_spawner_random
import ball_spawner_clustered

from std_msgs.msg import Bool
from std_srvs.srv import Empty
from gazebo_msgs.srv import GetModelState, GetWorldProperties, DeleteModel, SpawnModel

class progressTracker():
	
	def __init__(self, turtlebotCount, layout, testType, timeSteps, ballCount, testArea, clusterParameter, testCount, frequency, directoryName):
			
		#As per usual, start by initiating the ros node
		rospy.init_node("progress_tracker", anonymous=True)
		
		#Create a timer
		self.timer = rospy.Rate(int(frequency))
		
		#Generate a list of turtlebot model names
		self.turtlebotNames = []
		for i in range(int(turtlebotCount)):
			self.turtlebotNames.append("Turtlebot{}".format(i))
			
		#Create a publisher to reset the deliberative master node if necessary
		self.resetSignalPublisher = rospy.Publisher("deliberativeResetSignal", Bool, queue_size = 5)
		
		#Define a list of model names for targets
		self.targetModelBaseNames = ["Ball"]
		self.turtlebotCount = int(turtlebotCount)
				
		#Create a directory for this set of .csv files to be stored in
		baseDir = "../csv/"
		
		if not os.path.exists(baseDir + directoryName):
			print "Creating folder with name {} in {}..".format(directoryName, baseDir)	
		else:
			identifier = 2
			while os.path.exists(baseDir + directoryName + str(identifier)):
				identifier += 1
			directoryName = directoryName + str(identifier)
			print "Creating folder with name {} in {}..".format(directoryName, baseDir)
			
		os.makedirs(baseDir + directoryName)
		self.fileDir = baseDir + directoryName + "/"
		
		#Begin by printing a file with all the info on the set of tests
		self.printInfoFile(turtlebotCount, layout, testType, timeSteps, ballCount, testArea, testCount, frequency, clusterParameter)
		
		#Run the specified number of tests
		for i in range(testCount):
			
			#First, reset the world to return the turtlebots to their starting positions
			self.resetWorld()
			
			#Then, pause the world to keep the TurtleBots in place
			self.pausePhysics()
			
			#Then, remove any litter that is present
			self.clearLitter()
			
			#Now, spawn new litter according to the arguments given
			if layout == "grid":
				ball_spawner_grid.spawnGrid(testArea[0], testArea[1], int(testArea[2]), int(testArea[3]))
				
			elif layout == "uniform_random":
				ball_spawner_random.spawnRandom(ballCount, testArea[0], testArea[1])
			
			elif layout == "clustered":
				print "Producing clustered layout.."
				ball_spawner_clustered.spawnRandom(ballCount, testArea[0], testArea[1], clusterParameter)
			else:
				print "Invalid layout specified"
				return
			
			#If necessary, reset the deliberative control node
			if testType == "deliberative":
				self.resetDeliberativeMaster()
				
			elif testType == "reactive":
				#Should reset the turtlebots here, since the reactive ones have an element of state
				self.resetReactiveTurtlebots()
			
			#Unpause the world and log the data
			self.unpausePhysics()
			self.logPoints(i, timeSteps, frequency, turtlebotCount)
	
	def printInfoFile(self, turtlebotCount, layout, testType, timeSteps, ballCount, testArea, testCount, frequency, clusterParameter):
		#Prints a short file with all the parameters of the set of tests listed
		fileName = "testSetParameters.txt"
		
		print "Creating file with name {} in {}..".format(fileName, self.fileDir)
		
		#Open the file and a csv writer
		infoFile = open(self.fileDir+fileName, "w")
		infoWriter = csv.writer(infoFile, delimiter = "|", quoting=csv.QUOTE_NONE)
		
		#Print out all the parameters going into the set
		infoFile.write("TURTLEBOT COUNT = {}\n".format(turtlebotCount))
		infoFile.write("LAYOUT = {}\n".format(layout))
		infoFile.write("CLUSTER PARAMETER = {}\n".format(clusterParameter))
		infoFile.write("MOVEMENT TYPE = {}\n".format(testType))
		infoFile.write("TIME STEPS = {}\n".format(timeSteps))
		infoFile.write("BALL COUNT = {}\n".format(ballCount))
		infoFile.write("TEST AREA = {}\n".format(testArea))
		infoFile.write("TEST COUNT = {}\n".format(testCount))
		infoFile.write("FREQUENCY = {}\n".format(frequency))

		infoFile.close()
		

		
	def resetReactiveTurtlebots(self):
		#Create a publisher to each turtlebot, resetting the minimal internal state it does have
		for i in range(self.turtlebotCount):
			topicName = "/turtlebot{}/reset".format(i)
			resetPub = rospy.Publisher(topicName, Bool, queue_size=5)
			resetPub.publish(Bool())
		
	def resetDeliberativeMaster(self):
		#Publishes a signal to the deliberative master node to reset itself, recalculate routes for each turtlebot etc.
		print "Resetting deliberative control node.."
		
		#Create a message to trigger the reset
		message = Bool()
		message.data = True
		
		#Publish the message
		self.resetSignalPublisher.publish(message)
	
	def logPoints(self, testIndex, timesteps, frequency, turtlebotCount):
			
		#Create a file for output, NOT OVERWRITING EXISTING FILES but producing a new one instead
		fileString = "test{}".format(testIndex)
		fileName = fileString + ".csv"
			
		#Check if this file already exists, adding a number to the end if needed to generate a unique filename
		if not os.path.isfile(self.fileDir + fileName):
			print "Creating file with name {} in {}..".format(fileName, self.fileDir)	
		else:
			identifier = 2
			while os.path.isfile(self.fileDir + fileString + str(identifier) + ".csv"):
				identifier += 1
			fileName = fileString + str(identifier) + ".csv"
			print "Creating file with name {} in {}..".format(fileName, self.fileDir)
		
		#Open the file and a csv writer
		self.outputFile = open(self.fileDir+fileName, "w")
		self.outputWriter = csv.writer(self.outputFile, delimiter = "|", quoting=csv.QUOTE_NONE)
		
		#Print out a header stating number of turtlebots in the scenario, number of targets and timestep frequency
		headerString = "Turtlebots:{}, Max Timesteps:{}, Timesteps per second:{}\n".format(turtlebotCount, timesteps, frequency)
		self.outputFile.write(headerString)
		 
		#Loop here, running checkAndLog after a set number of timesteps
		self.timestep = 0
		self.running = True
		
		#Print the initial poses for the targets
		self.printInitialPoses()
		
		#Start off as running, this can be stopped from within the loop once all targets are reached or a max time limit
		while self.timestep < timesteps:
			self.checkAndLog()
			self.timestep +=1
			self.timer.sleep()
			
		print "Finished logging data."
	
	def printInitialPoses(self):
		#Start by checking the number of targets in the scene
		targetModelBaseNames = ["Ball"]
		
		#Create ros service proxies
		rospy.wait_for_service("/gazebo/get_world_properties")
		try:
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			modelStateProxy = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to call service", e
			return 0;		
		
		#Check if each model in the world is a potential target for pickup based on its model name
		self.targetCount = 0
		self.targetNames = []
		for modelName in worldProperties.model_names:
			for baseName in targetModelBaseNames:
				if modelName.find(baseName) != -1:
					self.targetCount += 1
					self.targetNames.append(modelName)
		
		#Create a list of turtlebot model names
		self.turtlebotModelNames = []
		for i in range(self.turtlebotCount):
			self.turtlebotModelNames.append("Turtlebot{}".format(i))
		
		#For the first timestep, record the locations of all targets, after this we just say which ones are still there since they don't move from their position
		if self.timestep == 0:
			targetLocations = []
			turtlebotLocations = []
			
			#Find all target locations
			for target in self.targetNames:
				rospy.wait_for_service("/gazebo/get_model_state")
				targetLocation = modelStateProxy(target, "world")
				targetLocations.append("(%.2f,%.2f)" % (targetLocation.pose.position.x, targetLocation.pose.position.y))
			
			#Find all turtlebot locations
			for turtlebot in self.turtlebotModelNames:
				rospy.wait_for_service("/gazebo/get_model_state")
				turtlebotLocation = modelStateProxy(turtlebot, "world")
				#Calculate rotation about the z axis
				quaternion = (turtlebotLocation.pose.orientation.x, turtlebotLocation.pose.orientation.y, turtlebotLocation.pose.orientation.z, turtlebotLocation.pose.orientation.w)
				rollPitchYaw = tf.transformations.euler_from_quaternion(quaternion)
				rotation = rollPitchYaw[2]
				turtlebotLocations.append("(%.2f, %.2f, %.2f)" % (turtlebotLocation.pose.position.x, turtlebotLocation.pose.position.y, rotation))
			
			#Print the header row if it's the first timestep
			nameList = self.turtlebotModelNames + self.targetNames
			nameList.append("Collected:")
			nameList.insert(0, "timestep")
			self.outputWriter.writerow(nameList)
			locationList = turtlebotLocations + targetLocations
			locationList.insert(0, self.timestep)
			self.outputWriter.writerow(locationList)
			
			return
				
	def checkAndLog(self):
		
		#Create ros service proxies and call world properties
		rospy.wait_for_service("/gazebo/get_world_properties")
		try:
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			modelStateProxy = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to call service", e
			return 0;		
		
		#Get the position of each turtlebot
		turtlebotLocations = []
		
		for turtlebot in self.turtlebotModelNames:
			rospy.wait_for_service("/gazebo/get_model_state")
			turtlebotLocation = modelStateProxy(turtlebot, "world")
			#Calculate rotation about the z axis
			quaternion = (turtlebotLocation.pose.orientation.x, turtlebotLocation.pose.orientation.y, turtlebotLocation.pose.orientation.z, turtlebotLocation.pose.orientation.w)
			rollPitchYaw = tf.transformations.euler_from_quaternion(quaternion)
			rotation = rollPitchYaw[2]
			turtlebotLocations.append("(%.2f, %.2f, %.2f)" % (turtlebotLocation.pose.position.x, turtlebotLocation.pose.position.y, rotation))
			
		#Now, check if each target is still in the scene by looking through the model list in the world properties message
		targetStillPresent = []
		
		#Keep a running total of number found
		targetsCollected = 0
		
		#Add to each string the status of each target
		for target in self.targetNames:
			if target in worldProperties.model_names:
				targetStillPresent.append("1")
			else:
				targetStillPresent.append("0")
				targetsCollected += 1
		
		#Show the final total at the end of the column
		targetStillPresent.append(str(targetsCollected))
				
		#Finally, print this info to the output file and flush
		combinedList = turtlebotLocations + targetStillPresent
		combinedList.insert(0, self.timestep)
		self.outputWriter.writerow(combinedList)
		self.outputFile.flush()
		
	def clearLitter(self):
		#Clears the gazebo environment of all litter objects that are present
		
		#Begin by creating a service proxy for each service 
		try:
			removeProxy = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to create service proxies", e
		
		#Scan through this list, check which are in the list of target names for litter and remove them
		for model in worldProperties.model_names:
			if self.targetName(model):
				removeProxy(model)
				
		print "Cleared models"
					
	def targetName(self, name):
		#Checks if a given models name denotes it as a potential target for pickup
		# necessary because gazebo generates unique names for objects in the scene
				
		#Check if the base name appears in the name string, for instance "coke_can" would appear in "coke_can_3" denoting that it is a target
		for baseName in self.targetModelBaseNames:
			if name.find(baseName) != -1:
				return True	
	
	def pausePhysics(self):
		#Pauses the gazebo simulation through a service call
		pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
		
		rospy.wait_for_service("/gazebo/pause_physics")
		
		pauseProxy()
		
	def unpausePhysics(self):
		#Unpauses
		unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
		
		rospy.wait_for_service("/gazebo/unpause_physics")
		
		unpauseProxy()
		
	def resetWorld(self):
		#Returns every object in the gazebo world to its initial spawn position
		
		#Define a service proxy and wait for it to be available
		resetProxy = rospy.ServiceProxy("gazebo/reset_world", Empty)
		rospy.wait_for_service("/gazebo/reset_world")
		
		print "Resetting TurtleBot poses.."
		
		#Trigger the proxy, returning the TurtleBots to their spawn position
		resetProxy()
			
def main(args):
	tracker = progressTracker(turtlebotCount=int(args[1]), layout=args[2], testType=args[3], timeSteps=int(args[4]), ballCount = int(args[5]), testArea = (float(args[6]), float(args[7]), float(args[8]), float(args[9])), clusterParameter = float(args[10]), testCount = int(args[11]), frequency=int(args[12]), directoryName=args[13])
	print "All tests completed."
	
if __name__=="__main__":
	main(sys.argv)
