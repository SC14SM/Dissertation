#!/usr/bin/env python

#Node to translate a number of direction vectors into movement of the turtlebot. 
# In this initial solution the turtlebot simply stops motionless, turns to face its direction vector then goes directly forward
#	Eventually replaced by smooth_movement_node.py 

import numpy as np
import rospy
import sys
import math 

from geometry_msgs.msg import Twist, Vector3

class stopTurnGoNode():
	
	def __init__(self, namespaceIndex):
		
		#Initiate the ros node
		rospy.init_node("stop_turn_go_node", anonymous = True)
		
		namespace = "turtlebot{}/".format(namespaceIndex)
		
		#Initiate variables
		self.latestVector = Vector3()
		self.vectorMagnitude = 0.0
		self.vectorBearing = 0.0
		
		#Subscribe to the vector topic
		vectorSubscriber = rospy.Subscriber(namespace+"movement_vector", Vector3, self.vectorCallback)
		
		#Instantiate a publisher for movement commands
		self.movementPublisher = rospy.Publisher(namespace+"/mobile_base/commands/velocity", Twist, queue_size=5)
		
		#Start the movement loop
		self.startMovementLoop()
		
		
	def vectorCallback(self, vector):
		#Called every time a new vector is published
		
		#Find the bearing and magnitude of the vector and store them
 		self.latestVector = vector 
 		self.vectorMagnitude = math.sqrt(math.pow(vector.y,2)+ math.pow(vector.x, 2))
 		
 		#Bearing is found by working out the dot product of the vector with a unit vector in direction x, over the dot product of the two vectors magnitudes
 		bearing = np.arccos(vector.x/self.vectorMagnitude)
		
		#Arccos will always give us a value in the range 0..pi which isn't always helpful, since we want to be able to distinguish between pi/2 clockwise
		# and pi/2 counterclockwise. We can solve by simply checking if the vector points to the left or the right of the turtlebot
		if vector.y < 0:
			bearing *= -1
		
		#Store the resulting bearing
		self.vectorBearing = bearing
		
		
	def startMovementLoop(self):
		#Publishes movement commands based on the bearing of the movement vector received
		#Once started, continues publishing movement commands until a stopping criteria is met
		
		running=True
		
		#Define a timer, used to sleep between issuing movement commands
		tenHz = rospy.Rate(10)
		
		#Define the speeds with which the turtlebot moves
		maxTurnRate = math.pi/4						#Maximum angular speed with which the turtlebot turns
		linearSpeed = 0.3					#Speed the turtlebot travels in a straight line
		angularTolerance = math.pi/16		#Amount of deviation from perfectly straight that will be accepted without adjusting
		
		#Create some twist messages for publishing
		movementMessage = Twist()
		
		while running:
			print self.vectorBearing
			#Check if the bearing is within tolerance and that magnitude is not zero
			if abs(self.vectorBearing) <= angularTolerance:				
				movementMessage.linear.x = linearSpeed
			else:
				movementMessage.linear.x = 0
			#Else, turn towards the desired direction with speed determined by how big the adjustment is
			
			turnSpeed = (self.vectorBearing/math.pi)*maxTurnRate
			movementMessage.angular.z = turnSpeed
			self.movementPublisher.publish(movementMessage)

			#Sleep for 1 cycle after publishing the message
			tenHz.sleep()
			
def main(args):
	node = stopTurnGoNode(args[1])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)

				
				
			
			
