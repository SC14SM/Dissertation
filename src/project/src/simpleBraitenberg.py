#!/usr/bin/env python

#Initial experimentation with Braitenberg vehicles, directly modulates wheel speeds rather than publishing a vector for the modular movement system.

#	Replaced by centroid_vector_node.py

import cv2
import rospy
import sys

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from math import pi
from geometry_msgs.msg import Twist

class simpleBraitenberg:
	
	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("Braitenberg_Node", anonymous=True)
		
		#Create a subscriber to the filteredImage topic
		filteredImageSubscriber = rospy.Subscriber("filteredImage", Image, self.filteredImageCallback)
		
		#Create a publisher to publish twist messages
		self.pub = rospy.Publisher('mobile_base/commands/velocity', Twist)

		#Create a cvBridge to work with the image data
		self.bridge = CvBridge()
		
	def filteredImageCallback(self, filteredImage):
		#Called every time the subscriber receives a new image from the image filtering node
		
		#Start by converting the image to cv2 format
		try:
			convertedImage = self.bridge.imgmsg_to_cv2(filteredImage, "mono8")
		except:
			print "Unable to convert image to cv2 format"
			
		#Now, split the image in two horizontally
		height, width = convertedImage.shape[:2]
		centreColumn = width/2
		
		leftImage = convertedImage[0:height-1, 0:centreColumn-1]
		rightImage = convertedImage[0:height-1, centreColumn:width-1]
		
		#Sum the values in each half
		leftSum = cv2.sumElems(leftImage)[0]
		rightSum = cv2.sumElems(rightImage)[0]
		
		#Use these pixel sums as the arguments in a function that determines wheel motion
		self.pixelValuesToTwist(leftSum, rightSum)
				
	def pixelValuesToTwist(self, left, right):
		#Takes a pair of pixel sums, and converts it to a single twist message based on the relative brightness of each side
		#Then, publishes that message to the robot base
		
		maxWheelSpeed = 0.8
	
		#Calculate ratio between sums
		if (left+right)==0:
			leftRatio = 0.5
			rightRatio = 0.5
		else:
			leftRatio = float(left)/(left+right)
			rightRatio = 1.0 - leftRatio

		#Convert these wheel speeds to a twist message
		wheelDistance = 0.23	#Distance between wheels
		wheelRadius = 0.035		#Wheel radius in metres (35mm)
		wheelCircumference = 2.0*pi*wheelRadius
		
		maxAngularVelocity = maxWheelSpeed/wheelCircumference 
		
		#Calculate individual wheel speeds, remembering that left wheel speed is dependent on sum in the RIGHT half of the image and vice versa
		rightSpeed = leftRatio*maxAngularVelocity
		leftSpeed = rightRatio*maxAngularVelocity
		
		#From these, calculate linear and angular velocities of the turtlebot
		linearVelocity = (leftSpeed*wheelRadius + rightSpeed*wheelRadius)/2
		angularVelocity = (leftSpeed*wheelRadius - rightSpeed*wheelRadius)/(2*wheelDistance)
		
		#Produce a twist message and publish it
		twistOutput = Twist()
		twistOutput.linear.x = linearVelocity
		twistOutput.angular.z = angularVelocity*-5
		
		self.pub.publish(twistOutput)
		print leftRatio, angularVelocity
		
def main(args):
	#If this is in the main path, create an instance
	braitenbergNode = simpleBraitenberg()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e

if __name__=="__main__":
	main(sys.argv)
