#!/usr/bin/env python

#	Node which searches the camera feed for green targets, and produces a mask in which any region that is green in the RGB
#	feed has a value of 255, and any region that is not has a value of 0.

# 	This node forms the heart of the initial vision system, designed to detect very simple targets (green spheres)
#	for the initial stages of the project. Its output is used by green_sphere_detector.py in the deliberative and random
#	search strategies, as well as used to calculate the gradient of targets in the reactive search strategy.

#	The counterpart to this node for the improved vision system is neural_target_filter.py

import cv2
import numpy as np
import rospy
import sys

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class greenFilter():
	
	bridge = None					#Converts raw image to cv2 format
	filteredImagePublisher = None 	#ROS publisher for the output image
	
	def __init__(self):
		
		#Initiate the ROS node
		rospy.init_node("Green_Filter", anonymous=True)
		
		#Instantiate the cv bridge, our subscriber and our publisher
		self.bridge = CvBridge()
		
		#Read in the raw image data from the kinect
		imageSubscriber = rospy.Subscriber("camera/rgb/image_raw", Image, self.imageCallback)
		
		#Publisher outputs the image, once it has been filtered such that only regions of interest remain
		self.filteredImagePublisher = rospy.Publisher("targetMaskedImage", Image, queue_size=1)
		
		
	def imageCallback(self, Image):
		#Called on the image data every time the subscriber reads in new data
		#Filters the image to produce a greyscale image with only potential targets remaining
		#In this simplified version, we are simply searching for green regions in the image
		
		try:
			#Convert the raw image into a format readable by openCV; then convert rgb to hue, saturation, value 
			convertedImage = self.bridge.imgmsg_to_cv2(Image, "bgr8")
			convertedImage = cv2.cvtColor(convertedImage, cv2.COLOR_BGR2HSV)
		except:
			print "Unable to convert raw image to hsv"
			
		#Now, threshold the image so that only green regions remain
		sensitivity = 5		#How large the variation in hue can be 
		greenHue = 60		#The target hue
		
		greenLowerThreshold = np.array([greenHue - sensitivity, 100, 100])
		greenUpperThreshold = np.array([greenHue + sensitivity, 255, 255])
		
		#This produces a binary image of regions that either are green (value 255) or are not (value 0)
		greenThresholdedBinaryImage = cv2.inRange(convertedImage, greenLowerThreshold, greenUpperThreshold)
		
		#Apply the binary mask to our image, producing an image suitable for display
		greenThresholdedImage = cv2.bitwise_and(convertedImage, convertedImage, mask = greenThresholdedBinaryImage)
		
		#Apply downsampling to the image for performance
		downsamplingFactor = 0.25
		downsampledImage = cv2.resize(greenThresholdedBinaryImage, (0,0), fx=downsamplingFactor, fy=downsamplingFactor, interpolation=cv2.INTER_NEAREST)
		
		#Publish the binary image for use by other nodes, 
		#(remembering to convert the opencv image to a ROS image message using our bridge)
		try:
			self.filteredImagePublisher.publish(self.bridge.cv2_to_imgmsg(downsampledImage))
		except CvBridgeError as e:
			print "Error publishing image", e
		
					
def main(args):
	#If this is the main path, launch an instance
	thresholder = greenFilter()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)		
		
		
