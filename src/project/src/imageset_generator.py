#!/usr/bin/env python

#	Automatically generates a set of positive training instances for the neural network in the improved vision system.
#	Does this by repeated spawning targets and objects of clutter in front of a turtlebot, then waiting for a bounding square around
#	the target to be published by the depth_image_clusterer_node. Once this bounding square is published, this node captures an image
#	of the target through the turtleBots camera and saves it.

import sys
import rospy
import math
import tf
import os
import time
import cv2
import random
import numpy as np

from cv_bridge import CvBridge, CvBridgeError
from gazebo_msgs.srv import GetWorldProperties, SetPhysicsProperties, SpawnModel, SpawnModelRequest, DeleteModel, SetModelState
from gazebo_msgs.msg import ModelState
from std_srvs.srv import Empty
from geometry_msgs.msg import *
from sensor_msgs.msg import PointCloud2, PointField, Image
from project.msg import bounding_squares
from tf.transformations import quaternion_from_euler

class ImageSetGenerator():
	
	#Define the list of items that will be included in the training set
	targetModelSet = ["../gazebo_objects/litter/cigarette_box/model-1_4.sdf", "../gazebo_objects/litter/cigarette_butt/model-1_4.sdf", "../gazebo_objects/litter/noodle_box/model-1_4.sdf", "../gazebo_objects/litter/styrofoam_box/model-1_4.sdf", "../gazebo_objects/litter/styrofoam_cup/model-1_4.sdf", "../gazebo_objects/litter/paper_ball/model-1_4.sdf","../gazebo_objects/litter/soda_bottle/model-1_4.sdf"]
	clutterModelSet = ["../gazebo_objects/environment_objects/cedar_bench/model-1_4.sdf", "../gazebo_objects/environment_objects/hardwood_bollard/model-1_4.sdf", "../gazebo_objects/environment_objects/planter/model-1_4.sdf", "../gazebo_objects/environment_objects/litter_bin/model-1_4.sdf"]
	wallModelPath = "../gazebo_objects/environment_objects/wall/model-1_4.sdf"
	
	#Some parameters to stop things spawning outside the FOV or inside eachother (causing weird collision bugs)
	modelMaxDistance = 3.75
	modelMinDistance = 2.0
	modelDistance = 0
	backgroundMinDistance = 1.0
	backgroundMaxDistance = 2.0
	backgroundMaxOffset = 0.25
	wallMinDistance = 2
	wallMaxDistance = 2
	
	#Keep track of how rotated the turtlebot is at any one point
	currentRotation = 0
	
	#Define how many images to generate
	imageSetSize = 1000

	def __init__(self):
		#Start by initiating a ROS node
		rospy.init_node("Image_set_generator", anonymous=True)

		#Wait for services to be ready
		rospy.wait_for_service("gazebo/get_world_properties")
		
		#Create a cv bridge
		self.bridge = CvBridge()
		
		self.tenHzTimer = rospy.Rate(10)
		
		#Read in the raw image data from the kinect
		imageSubscriber = rospy.Subscriber("turtlebot0/camera/rgb/image_raw", Image, self.imageCallback)
		depthSubscriber = rospy.Subscriber("turtlebot0/camera/depth/image_raw", Image, self.depthCallback)
		pointSubscriber = rospy.Subscriber("turtlebot0/camera/depth/points", PointCloud2, self.pointCloudCallback)
		
		#Subscribe to the boundingsquares topic published by the clusterer
		squaresSubscriber = rospy.Subscriber("turtlebot0/boundingSquares", bounding_squares, self.boundingSquaresCallback)
		
		#Initiate the image feeds
		self.latestPoints = PointCloud2()
		self.latestImage = Image()
		self.latestDepthImage = Image()
		self.latestBoundingSquares = bounding_squares()
		self.newImagePublished = False
		
		#Check if we want to randomly misalign the target within the image
		userInput = str(raw_input("Randomly misalign target within image? [y/n]"))
		if userInput == "y":
			self.misalignImage = True
		else: 
			self.misalignImage = False
		
		#Create a directory to save images in, each image set should go in its own uniquely named folder
		baseFileDir = "../training_data/positive_image_sets/"
		folderBaseName = "set"
		self.fileDir = baseFileDir + folderBaseName + "/"
		
		#If this directory doesn't already exist then make it
		if not os.path.exists(self.fileDir):
			os.makedirs(self.fileDir)
		#Otherwise, add some number to the end to make the directory name unique
		else:
			identifier = 2	
			while os.path.exists(baseFileDir + folderBaseName + str(identifier)):
				identifier += 1
			self.fileDir = baseFileDir + folderBaseName + str(identifier) + "/"
			os.makedirs(self.fileDir)
		
		print "Created directory {} for images..".format(self.fileDir)
		
		#Variable to keep track of number of images in the dataset generated
		self.generatedImages = 0
		self.baseFilename = "img"
		
		#Create service proxies for spawning and deleting models
		try:
			self.worldPropertiesProxy = rospy.ServiceProxy("gazebo/get_world_properties", GetWorldProperties)
			self.physicsPropertiesProxy = rospy.ServiceProxy("gazebo/set_physics_properties", SetPhysicsProperties)
			self.spawnModelProxy = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
			self.pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
			self.unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
			self.removeProxy = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)
			self.configurationProxy = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)
			
		except Exception, e:
			print "Unable to set up service proxies"
			print e
		
		#Read in the XML for each model once at the start, faster than doing it over and over again unnecessarily
		self.model_xml = []
		for i in range(len(self.targetModelSet)):
			with open(self.targetModelSet[i], "r") as f:
				xml = f.read()
				self.model_xml.append(xml)
		
		self.clutter_xml = []
		for i in range(len(self.clutterModelSet)):
			with open(self.clutterModelSet[i], "r") as f:
				xml = f.read()
				self.clutter_xml.append(xml)
		
		with open(self.wallModelPath, "r") as f:
			self.wallXML = f.read()
		
		#Start the main loop
		while self.generatedImages < self.imageSetSize:
			if not rospy.is_shutdown():
				self.spawnModel()
				self.randomiseBackground()
				self.captureImage()
				self.removeModel()
			else:
				print "Keyboard Interrupt detected."
				sys.exit()
				
		print "Full image set generated."

		
			
	def spawnModel(self):
		#Spawns a random model from the selected set in a random location within the turtlebots field of view
		# this assumes a turtlebot is located at 0,0 with no rotation
		
		#Begin by rotating the robot around the z axis at random
		turtlebotRotation = (random.random()*math.pi*2)-math.pi
		self.currentRotation = turtlebotRotation 						#Store for access by other functions
		quaternion = quaternion_from_euler(0, 0, turtlebotRotation)		#Get a random rotation as a quaternion
		
			
		#Form a message to send to the service
		configurationMessage = ModelState()
		configurationMessage.model_name = "Turtlebot0"
		configurationMessage.pose.orientation.x = quaternion[0]
		configurationMessage.pose.orientation.y = quaternion[1]
		configurationMessage.pose.orientation.z = quaternion[2]
		configurationMessage.pose.orientation.w = quaternion[3]
		
		#Call the service with this message			
		self.configurationProxy(configurationMessage)
		
		#Next, pick a model from the list at random
		modelIndex = int(math.floor(random.random()*len(self.targetModelSet)))
			
		#Pick a random distance and rotation
		rotation = (random.random()*math.pi*2)-math.pi
		distance = self.modelMinDistance + random.random()*(self.modelMaxDistance-self.modelMinDistance)
		
		#Generate a quaternion from the rotation about the z axis that we've chosen
		quaternion = quaternion_from_euler(0, 0, rotation)
			
		#Figure out the right x and y to put the target in front of the rotated turtlebot
		rotatedX = math.cos(turtlebotRotation)*distance
		rotatedY = math.sin(turtlebotRotation)*distance
				
		#Spawn the model in this pose
		rospy.wait_for_service("gazebo/spawn_sdf_model")
			
		#Format a spawn request
		request = SpawnModelRequest()
		request.initial_pose.position.x=rotatedX
		request.initial_pose.position.y=rotatedY
		request.initial_pose.position.z=0.0
		request.initial_pose.orientation.x = quaternion[0]
		request.initial_pose.orientation.y = quaternion[1]
		request.initial_pose.orientation.z = quaternion[2]
		request.initial_pose.orientation.w = quaternion[3]
		request.model_xml = self.model_xml[modelIndex]
		request.model_name = "Target"
			
		#Call the proxy to spawn the target
		if not self.spawnModelProxy(request):
			print "Unable to spawn model."
		
		#Store the distance from the centre that it was spawned, so we can spawn something behind it without it overlapping
		self.modelDistance = distance
		
	def randomiseBackground(self):
		#Pick a random item of clutter (including turtlebots) (or no clutter at all)
		#begin with a 50/50 chance to spawn no clutter
		clutterDistance = 0
		if random.random() > 0.5:
			
			#With a probability of 0.25 we use another turtlebot as the object of clutter
			if random.random() < 0.25:
				usingTurtleBotAsClutter = True
			else:
				usingTurtleBotAsClutter = False
			
			clutterIndex = int(math.floor(random.random()*len(self.clutterModelSet)))
			
			clutterDistance = self.modelDistance + self.backgroundMinDistance + random.random()*(self.backgroundMaxDistance-self.backgroundMinDistance)
			clutterOffset = ((random.random()*2)-1)*self.backgroundMaxOffset
			randomRotation = ((random.random()*2)-1)*math.pi
			
			#Convert the rotation into a quaternion
			quaternion = quaternion_from_euler(0, 0, randomRotation)
			
			#Figure out the right x and y to put it behind the target
			rotatedX = math.cos(self.currentRotation)*clutterDistance - math.sin(self.currentRotation)*clutterOffset 
			rotatedY = math.sin(self.currentRotation)*clutterDistance + math.cos(self.currentRotation)*clutterOffset
			
			#If we are using a turtlebot as the clutter, we move the second turtlebot in the world to the random position we just generated
			if usingTurtleBotAsClutter:
			
				#Create an empty modelstate to put the pose in
				turtleBotState = ModelState()
				turtleBotState.model_name = "Turtlebot1"
				turtleBotState.pose.position.x=rotatedX
				turtleBotState.pose.position.y=rotatedY
				turtleBotState.pose.orientation.y = quaternion[0]
				turtleBotState.pose.orientation.y = quaternion[1]
				turtleBotState.pose.orientation.z = quaternion[2]
				turtleBotState.pose.orientation.w = quaternion[3]
			
				#Pass this state to the service which moves objects
				if not self.configurationProxy(turtleBotState):
					print "Unable to relocate turtleBot"
		
			#Otherwise, we spawn an ordinary object at this location
			else:
				
				#Format a spawn request
				request = SpawnModelRequest()
				request.initial_pose.position.x=rotatedX
				request.initial_pose.position.y=rotatedY
				request.initial_pose.position.z=0.0
				request.initial_pose.orientation.x = quaternion[0]
				request.initial_pose.orientation.y = quaternion[1]
				request.initial_pose.orientation.z = quaternion[2]
				request.initial_pose.orientation.w = quaternion[3]
				request.model_xml = self.clutter_xml[clutterIndex]
				request.model_name = "Clutter"
				
				#Call the proxy to spawn the target
				if not self.spawnModelProxy(request):
					print "Unable to spawn model."
			
		#Spawn a wall behind the target at some distance with probability 0.5
		if random.random() > 0.5:
			wallDistance = self.wallMinDistance + random.random()*(self.wallMaxDistance-self.wallMinDistance)
			
			#Convert the rotation into a quaternion
			quaternion = quaternion_from_euler(0, 0, (math.pi/2)+self.currentRotation)
				
			#Figure out the right x and y to put it behind the target
			rotatedX = math.cos(self.currentRotation)*(wallDistance+clutterDistance+self.modelDistance)
			rotatedY = math.sin(self.currentRotation)*(wallDistance+clutterDistance+self.modelDistance)
			
			#Format a spawn request
			request = SpawnModelRequest()
			request.initial_pose.position.x=rotatedX
			request.initial_pose.position.y=rotatedY
			request.initial_pose.position.z=0.0
			request.initial_pose.orientation.x = quaternion[0]
			request.initial_pose.orientation.y = quaternion[1]
			request.initial_pose.orientation.z = quaternion[2]
			request.initial_pose.orientation.w = quaternion[3]
			request.model_xml = self.wallXML
			request.model_name = "Wall"
			
			#Call the proxy to spawn the wall
			if not self.spawnModelProxy(request):
				print "Unable to spawn model."
		
		
	def removeModel(self):
		#Removes the model produced by spawnModel(), so that the scene is cleaned before the next image is captured
		#This is easily done, since we always assign the same name to the model which is all we need to know to remove it
		
		#Wait for the service and create a proxy
		rospy.wait_for_service("gazebo/delete_model")
		removeProxy = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)

		#Call the proxy to remove the item
		removeProxy("Target")
		
		#Also, try to remove the background clutter - may return false if none exists but thats fine
		removeProxy("Clutter")
		removeProxy("Wall")
		
		#Finally, reset the second turtlebot to the corner of the scene out of the way
		#Create an empty modelstate to put the pose in
		turtleBotState = ModelState()
		turtleBotState.model_name = "Turtlebot1"
		turtleBotState.pose.position.x=15
		turtleBotState.pose.position.y=15
			
		#Pass this state to the service which moves objects
		if not self.configurationProxy(turtleBotState):
			print "Unable to relocate turtleBot"
		
		
		
	def captureImage(self):
		#Captures an image of the spawned model 
		
		#First, wait for a new image to be published to make sure that the newly spawned model is actually visible in the camera feed
		
		while not self.newImagePublished:
			self.tenHzTimer.sleep()
			
		#Reset the variable to false before moving on
		self.newImagePublished = False
				
		#From the bounding squares topic we may get a number of objects, but we only want the square that contains the object 
		for i in range(20):
			self.tenHzTimer.sleep()
		squares = self.latestBoundingSquares
		
		#Wait for at least one bounding square to be found in the image, giving up after a certain time
		maxWait = 10
		waitCount = 0
		while squares.count == 0:
			self.tenHzTimer.sleep()
			waitCount += 1
			if waitCount >= maxWait:
				print "Unable to find any bounding squares in image - moving on without capturing"
				return
			else:
				squares = self.latestBoundingSquares
		
		#Search through all the squares to find the one that is closest in the depth field - that is our area to crop out
		# check the depth feed value for the central pixel in that square
		closestSquareIndex = 0
			
		if squares.count > 1:	#If more than one is found, we go into this little loop to find the closest
			
			#Convert the latest depth image to a usable format
			depthImageConverted = self.bridge.imgmsg_to_cv2(self.latestDepthImage)
				
			#Find the square with the lowest depth value for its central pixel
			closestSquareDistance = 999
			for i in range(squares.count):
				cx = (squares.left_pixel_indexes[i] + (squares.side_lengths[i]/2))
				cy = (squares.top_pixel_indexes[i] + (squares.side_lengths[i]/2))
				
				#Get these pixel values for the non-downsampled image
				scalingFactor = 0.25
				cx = int(cx/scalingFactor)
				cy = int(cy/scalingFactor)
				
				if float(depthImageConverted[cx][cy]) < closestSquareDistance:
					closestSquareIndex = i
					closestSquareDistance = depthImageConverted[cx][cy]
		
		#If the user selected this option at the start, misalign the target within the image so that the bounding square no longer perfectly encloses it
		if self.misalignImage:
			
			#Randomly choose a scale factor for the side length of the bounding square to use, up to some maximum multiple of the perfectly aligned bounding square's size
			maxScaleFactor = 4
			randomScaleFactor = 0.5 + random.random()*(maxScaleFactor-0.5)
			
			#Now, choose how the perfectly aligned bounding square is placed within this larger square
			#Begin by calculating the top and left sides of the box
			extraSpace = (squares.side_lengths[closestSquareIndex]*randomScaleFactor) - squares.side_lengths[closestSquareIndex]
			leftSpace = math.floor(random.random()*extraSpace)
			topSpace = math.floor(random.random()*extraSpace)
		
			#Avoid array indexing errors here
			if squares.left_pixel_indexes[closestSquareIndex]-leftSpace < 0:
				leftSpace = squares.left_pixel_indexes[closestSquareIndex]
			if squares.top_pixel_indexes[closestSquareIndex]-topSpace < 0:
				topSpace = squares.top_pixel_indexes[closestSquareIndex]
			
			xStart = int(squares.left_pixel_indexes[closestSquareIndex]-leftSpace)
			yStart = int(squares.top_pixel_indexes[closestSquareIndex]-topSpace)
			
			#Calculate the right and bottom sides now, avoiding array indexing errors
			scaledSideLength = math.floor(randomScaleFactor*squares.side_lengths[closestSquareIndex])
			
			if xStart+scaledSideLength >= 160:
				scaledSideLength = 160-(xStart+1)
			if yStart+scaledSideLength >= 120:
				scaledSideLength = 120-(yStart+1)
				
			xEnd = int(xStart + scaledSideLength)
			yEnd = int(yStart + scaledSideLength)
			 		
		#Otherwise, use a square in which the target is perfectly enclosed
		else:
			xStart = squares.left_pixel_indexes[closestSquareIndex]
			xEnd = squares.left_pixel_indexes[closestSquareIndex] + squares.side_lengths[closestSquareIndex]
			yStart = squares.top_pixel_indexes[closestSquareIndex]
			yEnd = squares.top_pixel_indexes[closestSquareIndex] + squares.side_lengths[closestSquareIndex]
		
		#Get the latest RGB image via cvbridge
		try:
			#Convert the raw image into a format readable by openCV; then convert rgb to hue, saturation, value 
			convertedImage = self.bridge.imgmsg_to_cv2(self.latestImage)
		except:
			print "Unable to convert raw image to hsv"
			
		#Convert these coordinates to work in the undownsampled image
		scalingFactor = 0.25
		xStart = int(xStart*(1/scalingFactor))
		xEnd = int(xEnd*(1/scalingFactor))
		yStart = int(yStart*(1/scalingFactor))
		yEnd = int(yEnd*(1/scalingFactor))
		croppedImage = convertedImage[xStart:xEnd, yStart:yEnd]
			
		#Resize the image for the classifier
		desiredSize = (32,32)
		resizedImage = cv2.resize(croppedImage, desiredSize, interpolation=cv2.INTER_LINEAR)

		#Generate a filename and check that it's unique
		fileName = self.baseFilename + str(self.generatedImages)
			
		if not os.path.isfile(self.fileDir + fileName):
			#Save the image
			cv2.imwrite("{}.png".format(self.fileDir+fileName), resizedImage)
			print "Created image {} in {}..".format(fileName, self.fileDir)
		else:
			print "Problem writing image, {} already exists in {}..".format(fileName, self.fileDir)
			
		#Increment the count of generated images
		self.generatedImages += 1
		
	def pointCloudCallback(self, points):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestPoints = points
		
	def imageCallback(self, image):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestImage = image
		self.newImagePublished = True
	
	def boundingSquaresCallback(self, squaresMessage):
		#Just store the latest set of squares for use elsewhere
		self.latestBoundingSquares = squaresMessage
		
	def depthCallback(self, image):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestDepthImage = image
		
def main(args):
	imageGenerator = ImageSetGenerator()
	
if __name__=="__main__":
	main(sys.argv)
