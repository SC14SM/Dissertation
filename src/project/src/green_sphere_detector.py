#!/usr/bin/env python

#	Node which searches the camera feed for targets, and reports if they are present
# 	Does not handle the details of navigating to these targets, simply publishes a vector pointing towards the nearest target
#	when it spots one.

# 	This node is designed to detect very simple targets (green spheres), for the initial stages of the project, and relies on 
#	a colour thresholding technique. 

#	The counterpart to this node in the improved vision system is neural_detector_node.py

import cv2
import numpy as np
import rospy
import sys
import struct

from sensor_msgs.msg import PointCloud2, PointField, Image
from geometry_msgs.msg import Vector3
from cv_bridge import CvBridge, CvBridgeError

class greenDetector():
	
	bridge = None					#Converts raw image to cv2 format
	filteredImagePublisher = None 	#ROS publisher for the output image
	
	def __init__(self, namespaceIndex):
		
		#Initiate the ROS node
		rospy.init_node("Green_detector", anonymous=True)
		
		#Define a downsampling factor here, to keep it consistent between the depth and rgb feeds
		self.downsamplingFactor = 0.25
		
		#Instantiate the cv bridge, our subscriber and our publisher
		self.bridge = CvBridge()
		
		#Instantiate the infeed storage
		self.depthImagePublished = False
		self.pointsPublished = False
		self.imagePublished = False
		
		#Create a publisher for the vector output
		self.vectorPublisher = rospy.Publisher("target_vector", Vector3, queue_size = 5)
		
		#Read in the raw image data from the kinect
		imageSubscriber = rospy.Subscriber("camera/rgb/image_raw", Image, self.imageCallback)
		depthSubscriber = rospy.Subscriber("camera/depth/image_raw", Image, self.depthCallback)
		pointSubscriber = rospy.Subscriber("camera/depth/points", PointCloud2, self.pointCloudCallback)
		
		#Start a loop, generating vectors at some frequency
		maxPublishFrequency = 5
		timer = rospy.Rate(maxPublishFrequency)
		
		while not rospy.is_shutdown():
			self.generateVector()
			timer.sleep()

	def pointCloudCallback(self, points):
		#Called when the subscriber receives new data
		# just stores the points so that the required points can be extracted as needed
		self.latestPoints = points
		self.pointsPublished = True
		
	def depthCallback(self, depthImage):
		#Simply takes the depth image and stores it in a variable every time a new one is published
		self.latestDepthImage = depthImage
		self.depthImagePublished = True

	def imageCallback(self, Image):
		#Called on the image data every time the subscriber reads in new data
		#Simply converts and downsamples the image, storing it
		self.latestImage = Image
		self.imagePublished = True
	
		
	def generateVector(self):
		#Accepts a filtered depth image, with all regions that are not green in the rgb image excluded (set to zero)
		# From this, it checks if the closest point in the depth image is within some range, and if so publishes a vector 
		# in the turtlebot's coordinate frame pointing directly toward the point in question
		
		#First we check that all the subscribers have read in some data already:
		allFeedsPublished = self.pointsPublished and self.depthImagePublished and self.imagePublished
		
		if allFeedsPublished:
			
			#Begin by converting the various images into usable formats, also storing them so that they don't change while we are using them
			try:
				#Convert the raw images into a format readable by openCV; then convert rgb to hue, saturation, value 
				convertedImage = self.bridge.imgmsg_to_cv2(self.latestImage, "bgr8")
				convertedImage = cv2.cvtColor(convertedImage, cv2.COLOR_BGR2HSV)
				convertedDepthImage = self.bridge.imgmsg_to_cv2(self.latestDepthImage)
				
				#Downsample the images for performance
				downsampledImage = cv2.resize(convertedImage, (0,0), fx=self.downsamplingFactor, fy=self.downsamplingFactor, interpolation=cv2.INTER_NEAREST)
				downsampledDepthImage = cv2.resize(convertedDepthImage, (0,0), fx=self.downsamplingFactor, fy=self.downsamplingFactor, interpolation=cv2.INTER_NEAREST)

			except:
				print "Unable to convert raw images to usable format"
			
			#Threshold the image so that only green regions remain
			sensitivity = 5		#How large the variation in hue can be 
			greenHue = 60		#The target hue
		
			greenLowerThreshold = np.array([greenHue - sensitivity, 100, 100])
			greenUpperThreshold = np.array([greenHue + sensitivity, 255, 255])
			
			#This produces a binary image of regions that either are green (value 255) or are not (value 0)
			greenThresholdedBinaryImage = cv2.inRange(downsampledImage, greenLowerThreshold, greenUpperThreshold)
		
			#Use this binary image to exclude regions of the depth image that do not show up as green on the rgb image
			filteredImage = cv2.bitwise_and(downsampledDepthImage, downsampledDepthImage, mask = greenThresholdedBinaryImage)
			
			if (len(downsampledDepthImage[np.nonzero(filteredImage)]) == 0):
				#Catch the possibility that no non-zero pixels are left in the masked depth image, publish empty vector if so
				self.vectorPublisher.publish(Vector3(0,0,0))
			else:
				#Find the minimum depth value in this masked image
				minDepth = np.nanmin(downsampledDepthImage[np.nonzero(filteredImage)])
				
				#Locate the minimum depth pixel in the image
				for i in range(downsampledDepthImage.shape[0]):
					for j in range(downsampledDepthImage.shape[1]):
						if downsampledDepthImage[i][j] == minDepth:
							#Find the right entry in the non-downsampled pointcloud feed
							scalingFactor = 0.25
							pointCloudi = int(i/scalingFactor)
							pointCloudj = int(j/scalingFactor)
					
							#Get the x,y,z distances for this pixel from the pointcloud feed
							start = (pointCloudj*self.latestPoints.point_step) + (pointCloudi*self.latestPoints.row_step)
							xOffset = 0
							yOffset = 4		#Offset needed to get the y value for this point
							zOffset = 8
							
							#Get the float values from the byte array
							try:
								#Unpack the x and y values from the point cloud feed
								yValue = struct.unpack_from("f", self.latestPoints.data, start+xOffset)[0]
								xValue = struct.unpack_from("f", self.latestPoints.data, start+zOffset)[0]
								
								#Publish a vector, and return since we only want to publish at most a single vector
								# We make the y value negative to keep with the convention used elsewhere that y value increases to the left
								self.vectorPublisher.publish(Vector3(xValue, -yValue, 0))
							except Exception, e:
								print "Green sphere detector unable to publish vector:", e
							
							#Once we've found our point and published the vector, we return because we don't need to check the rest of the image
							return
							
		#If the necessary topics haven't had anything published on them yet, we just publish an empty vector
		else:
			self.vectorPublisher.publish(Vector3(0,0,0))
		
def main(args):
	#If this is the main path, launch an instance
	detector = greenDetector(args[1])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)		
		
		
