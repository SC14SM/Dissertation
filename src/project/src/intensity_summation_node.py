#!/usr/bin/env python

#	Node which subscribes to an image feed with areas containing targets highlighted, then publishes a summation of the 
# 	total brightness in the image.
# 	Used to calculate if the gradient of rubbish is increasing or decreasing.
# 	Works on an image feed regardless of how that image is calculated - ie, should work with a placeholder vision solution or the more complex one

#	Used by the reactive search with both vision systems, listening to the output of either green_filter.py or neural_target_filter.py, but 
#	working exactly the same regardless of which node is producing the input to this one.

import cv2
import numpy as np
import rospy
import sys

from std_msgs.msg import Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class intensities_summer():
	
	def __init__(self):
		
		#Start as always by initiating the ros node
		rospy.init_node("intensity_summation_node", anonymous = True)
		
		#Create an openCV bridge to deal with the image message
		self.bridge = CvBridge()
		
		#Create a publisher to output the results
		self.outputPublisher = rospy.Publisher("summed_intensities", Float64, queue_size=5)
		
		#Create a publisher to the image topic we want
		imageSubscriber = rospy.Subscriber("targetMaskedImage", Image, self.imageCallback)
		
	def imageCallback(self, imageMessage):
		#Sums the pixel values in the single channel image published
		
		#Begin by converting the image to openCV readable format
		try:
			imageConverted = self.bridge.imgmsg_to_cv2(imageMessage)
		except Exception as e:
			print "Intensity summer unable to convert image to openCV format.", e
		 
		 #Simply skim through the image top left to bottom right summing values
		runningTotal = 0
		for i in range(imageMessage.height):
			for j in range(imageMessage.width):
				runningTotal += imageConverted[i][j]
				 
		#Produce a message for publishing
		outputMessage = Float64()
		outputMessage.data = runningTotal
		
		self.outputPublisher.publish(outputMessage)
		
def main(args):
	#If this is the main path, launch an instance
	intensities = intensities_summer()
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
		
if __name__=="__main__":
	main(sys.argv)		
				 
	
