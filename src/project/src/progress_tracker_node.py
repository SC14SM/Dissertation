#!/usr/bin/env python

#	Node to keep track of how many pieces of rubbish/targets have been sucessfully removed, along with 
#	the location of each turtlebot at fixed timesteps. Will be used to evaluate the performance of various
#	configurations, and to produce graphs and figures for the final report.
#
#	Outputs to a .csv file for ease of reference and further analysis

import numpy as np
import rospy
import sys
import math 
import os
import csv
import tf

from gazebo_msgs.srv import GetModelState, GetWorldProperties

class progressTracker():
	
	def __init__(self, turtlebotCount, frequency, fileString):
			
		#As per usual, start by initiating the ros node
		rospy.init_node("progress_tracker", anonymous=True)
		
		#Create a timer
		self.timer = rospy.Rate(int(frequency))
		
		#Generate a list of turtlebot model names
		self.turtlebotNames = []
		for i in range(int(turtlebotCount)):
			self.turtlebotNames.append("Turtlebot{}".format(i))
		
		#Define a list of model names for targets
		self.targetNames = []
		self.turtlebotCount = int(turtlebotCount)
		
		#Create a file for output, NOT OVERWRITING EXISTING FILES but producing a new one instead
		fileDir = "../csv/"
		fileName = fileString + ".csv"
		
		if not os.path.exists(fileDir):
			os.makedirs(fileDir)
			
		#Check if this file already exists, adding a number to the end if needed to generate a unique filename
		if not os.path.isfile(fileDir + fileName):
			print "Creating file with name {} in {}..".format(fileName, fileDir)	
		else:
			identifier = 2
			while os.path.isfile(fileDir + fileString + str(identifier) + ".csv"):
				identifier += 1
			fileName = fileString + str(identifier) + ".csv"
			print "Creating file with name {} in {}..".format(fileName, fileDir)
		
		#Open the file and a csv writer
		self.outputFile = open(fileDir+fileName, "w")
		self.outputWriter = csv.writer(self.outputFile, delimiter = "|", quoting=csv.QUOTE_NONE)
		
		#Print out a header stating number of turtlebots in the scenario, number of targets and timestep frequency
		headerString = "Turtlebots:{}, Timesteps per second:{}\n".format(turtlebotCount, frequency)
		self.outputFile.write(headerString)
		 
		#Loop here, running checkAndLog after a set number of timesteps
		self.timestep = 0
		self.running = True
		
		#Print the initial poses for the targets
		self.printInitialPoses()
		
		#Start off as running, this can be stopped from within the loop once all targets are reached or a max time limit
		while self.timestep < 50:
			self.checkAndLog()
			self.timestep +=1
			self.timer.sleep()
			
		print "Finished logging data."
	
	def printInitialPoses(self):
		#Start by checking the number of targets in the scene
		targetModelBaseNames = ["Ball"]
		
		#Create ros service proxies
		rospy.wait_for_service("/gazebo/get_world_properties")
		try:
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			modelStateProxy = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to call service", e
			return 0;		
		
		#Check if each model in the world is a potential target for pickup based on its model name
		self.targetCount = 0
		self.targetNames = []
		for modelName in worldProperties.model_names:
			for baseName in targetModelBaseNames:
				if modelName.find(baseName) != -1:
					self.targetCount += 1
					self.targetNames.append(modelName)
		
		#Create a list of turtlebot model names
		self.turtlebotModelNames = []
		for i in range(self.turtlebotCount):
			self.turtlebotModelNames.append("Turtlebot{}".format(i))
		
		#For the first timestep, record the locations of all targets, after this we just say which ones are still there since they don't move from their position
		if self.timestep == 0:
			targetLocations = []
			turtlebotLocations = []
			
			#Find all target locations
			for target in self.targetNames:
				rospy.wait_for_service("/gazebo/get_model_state")
				targetLocation = modelStateProxy(target, "world")
				targetLocations.append("(%.2f,%.2f)" % (targetLocation.pose.position.x, targetLocation.pose.position.y))
			
			#Find all turtlebot locations
			for turtlebot in self.turtlebotModelNames:
				rospy.wait_for_service("/gazebo/get_model_state")
				turtlebotLocation = modelStateProxy(turtlebot, "world")
				#Calculate rotation about the z axis
				quaternion = (turtlebotLocation.pose.orientation.x, turtlebotLocation.pose.orientation.y, turtlebotLocation.pose.orientation.z, turtlebotLocation.pose.orientation.w)
				rollPitchYaw = tf.transformations.euler_from_quaternion(quaternion)
				rotation = rollPitchYaw[2]
				turtlebotLocations.append("(%.2f, %.2f, %.2f)" % (turtlebotLocation.pose.position.x, turtlebotLocation.pose.position.y, rotation))
			
			#Print the header row if it's the first timestep
			nameList = self.turtlebotModelNames + self.targetNames
			nameList.insert(0, "timestep")
			self.outputWriter.writerow(nameList)
			locationList = turtlebotLocations + targetLocations
			locationList.insert(0, self.timestep)
			self.outputWriter.writerow(locationList)
			
			return
				
	def checkAndLog(self):
		
		#Create ros service proxies and call world properties
		rospy.wait_for_service("/gazebo/get_world_properties")
		try:
			worldPropertiesProxy = rospy.ServiceProxy("/gazebo/get_world_properties", GetWorldProperties)
			modelStateProxy = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
			worldProperties = worldPropertiesProxy()
		
		except rospy.ServiceException, e:
			print "Unable to call service", e
			return 0;		
		
		#Get the position of each turtlebot
		turtlebotLocations = []
		
		for turtlebot in self.turtlebotModelNames:
			rospy.wait_for_service("/gazebo/get_model_state")
			turtlebotLocation = modelStateProxy(turtlebot, "world")
			#Calculate rotation about the z axis
			quaternion = (turtlebotLocation.pose.orientation.x, turtlebotLocation.pose.orientation.y, turtlebotLocation.pose.orientation.z, turtlebotLocation.pose.orientation.w)
			rollPitchYaw = tf.transformations.euler_from_quaternion(quaternion)
			rotation = rollPitchYaw[2]
			turtlebotLocations.append("(%.2f, %.2f, %.2f)" % (turtlebotLocation.pose.position.x, turtlebotLocation.pose.position.y, rotation))
			
		#Now, check if each target is still in the scene by looking through the model list in the world properties message
		targetStillPresent = []
		
		for target in self.targetNames:
			if target in worldProperties.model_names:
				targetStillPresent.append("1")
			else:
				targetStillPresent.append("0")
				
		#Finally, print this info to the output file and flush
		combinedList = turtlebotLocations + targetStillPresent
		combinedList.insert(0, self.timestep)
		self.outputWriter.writerow(combinedList)
		self.outputFile.flush()
		
		
def main(args):
	tracker = progressTracker(args[1], args[2], args[3])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)
