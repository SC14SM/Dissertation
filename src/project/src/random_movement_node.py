#!/usr/bin/env python

#Publishes a random direction vector with fixed magnitude of 1. Used to introduce some randomness for avoidance of local minimas
# when navigating. Provides very similar functionality to brownian.py, but structured to fit into modular architecture, for use
# with a control node whereas brownian.py just publishes directly to the base 

import numpy as np
import rospy
import sys
import math 
import random

from geometry_msgs.msg import Twist, Vector3

class randomMovementNode():
	
	def __init__(self, namespaceIndex, turnProb = 0.05, frequency = 10):
		#Initiate ros node
		rospy.init_node("random_movement_vector", anonymous=True)
		
		#Read in constructor arguments
		self.perTimestepTurnChance = float(turnProb)
		self.topicName="random_vector"
		
		#Instantiate the required objects
		self.timer = rospy.Rate(int(frequency))
		self.publisher = rospy.Publisher(self.topicName, Vector3, queue_size=5)
		
		#Generate a random initial vector and publish it
		self.outputVector = Vector3(1,0,0)
		self.generateInitialVector()
		
		while not rospy.is_shutdown():
			#Repeatedly call a function to change the vector direction with some probability
			#Function includes a sleep call so that it gets called at most with the given frequency
			self.updateVector()
			self.timer.sleep()
	
	def generateInitialVector(self):
		#Generate and publish a random direction vector of unit length
		randomRotation = random.random()*math.pi*2 
		self.outputVector.x = np.cos(randomRotation)
		self.outputVector.y = np.sin(randomRotation)
		self.publisher.publish(self.outputVector)
	
	def updateVector(self):
		#Generate a random number and compare to the probability given to the constructor
		generated = random.random()
		
		if generated <= self.perTimestepTurnChance:
			#Generate a random bearing in the range 0..pi
			randomRotation = random.random()*math.pi*2 
			
			#Replace the output vector with a vector with this bearing and magnitude 1
			self.outputVector.x = np.cos(randomRotation)
			self.outputVector.y = np.sin(randomRotation)
		
			#Publish the output vector if a new one has been generated
			self.publisher.publish(self.outputVector)
			

def main(args):
	randomNode = randomMovementNode(args[1], args[2], args[3])
	
	try:
		rospy.spin()
	except Exception as e:
		print "Exception with rospy.spin()"
		print e
	
if __name__=="__main__":
	main(sys.argv)

