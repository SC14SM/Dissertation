#!/usr/bin/env python

#	Calls the functions defined in the various ball spawning scripts,
#	Included here as an independent node for use with the demo launch files.
#	Generally this functionality is carried out through automated_test.py,
#	but in this instance we only want to spawn the balls once, and aren't
#	concerned with logging of results.

import numpy as np
import rospy
import sys
import math 
import os
import csv
import tf
import ball_spawner_grid
import ball_spawner_random
import ball_spawner_clustered

from std_msgs.msg import Bool
from std_srvs.srv import Empty
from gazebo_msgs.srv import GetModelState, GetWorldProperties, DeleteModel, SpawnModel

class ballSpawner():
	
	def __init__(self, layout):
			
		#As per usual, start by initiating the ros node
		rospy.init_node("ball_spawner", anonymous=True)
		
		#Reset and pause the simulation
		self.resetWorld()
		self.pausePhysics()
		
		#Now, spawn new litter according to the arguments given
		if layout == "grid":
			ball_spawner_grid.spawnGrid(20, 20, 5, 5)
			
		elif layout == "uniform_random":
			ball_spawner_random.spawnRandom(25, 20, 20)
			
		elif layout == "clustered":
			print "Producing clustered layout.."
			ball_spawner_clustered.spawnRandom(25, 20, 20, 0.7)
		else:
			print "Invalid layout specified"
			return
	
		#Unpause the simulation
		self.unpausePhysics()

	def pausePhysics(self):
		#Pauses the gazebo simulation through a service call
		pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
		
		rospy.wait_for_service("/gazebo/pause_physics")
		
		pauseProxy()
		
	def unpausePhysics(self):
		#Unpauses
		unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
		
		rospy.wait_for_service("/gazebo/unpause_physics")
		
		unpauseProxy()
		
	def resetWorld(self):
		#Returns every object in the gazebo world to its initial spawn position
		
		#Define a service proxy and wait for it to be available
		resetProxy = rospy.ServiceProxy("gazebo/reset_world", Empty)
		rospy.wait_for_service("/gazebo/reset_world")
		
		print "Resetting TurtleBot poses.."
		
		#Trigger the proxy, returning the TurtleBots to their spawn position
		resetProxy()
			
def main(args):
	node = ballSpawner(args[1])
	
if __name__=="__main__":
	main(sys.argv)
