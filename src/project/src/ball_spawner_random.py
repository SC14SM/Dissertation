#!/usr/bin/env python

#	Spawns balls into the gazebo environment according to a uniform random layout.
#	Area in which to spawn them is variable, as well as the number of balls to spawn.
#	Primarily used by the automated testing node.

import sys
import rospy
import math
import tf
from random import random

from gazebo_msgs.srv import GetWorldProperties, SetPhysicsProperties, SpawnModel, SpawnModelRequest
from std_srvs.srv import Empty
from geometry_msgs.msg import *

def spawnRandom(ballCount, xDimension, yDimension):
	
	#Some variables that describe the layout
	areaDimensions  =   [xDimension, yDimension]		#Dimensions of the rectangular area in which balls should be spawned	
	centreRadius = 2.5									#Keep a small circular region in the centre clear so that no balls spawn inside the turtlebots
	minimumBallDistance = 0.5							#Minimum distance the balls can be from eachother, stops weird physics results if one overlaps with another

	#Start by initiating a ROS node
	#rospy.init_node("Ball_Spawner", anonymous=True)

	#Wait for services to be ready
	rospy.wait_for_service("gazebo/get_world_properties")

	#Create service proxies
	try:
		worldPropertiesProxy = rospy.ServiceProxy("gazebo/get_world_properties", GetWorldProperties)
		physicsPropertiesProxy = rospy.ServiceProxy("gazebo/set_physics_properties", SetPhysicsProperties)
		spawnModelProxy = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
		pauseProxy = rospy.ServiceProxy("gazebo/pause_physics", Empty)
		unpauseProxy = rospy.ServiceProxy("gazebo/unpause_physics", Empty)
	except Exception, e:
		print "Unable to set up service proxies"
		print e
		
	#Generate a set of coordinates, checking that the balls are non-intersecting and don't fall within a region in the very centre where the robots will be

	def inAcceptableRegion(coords, r):
		#Returns true so long as the coords provided aren't within a circle of radius r centred at 0,0
		# or within 0.5 metres of the boundary
		
		if math.sqrt(pow(coords[0],2) + pow(coords[1],2)) < r:
			return False
		elif abs(coords[0]) > (areaDimensions[0]/2)-0.5 or abs(coords[1]) > (areaDimensions[1]/2)-0.5:
			return False
		else:
			return True

	def notIntersecting(newCoords, existingCoords, minimumDistance):
		#Checks that the new set of coords provided don't fall within a minimum distance of an existing set
		#returns true or false
		
		for existing in existingCoords:
			distance = math.sqrt(pow((newCoords[0]-existing[0]),2) + pow((newCoords[1]-existing[1]),2))
			if distance < minimumDistance:
				return False
		
		return True

	#Now, produce the set of coords using these two methods to check they're correct
	coordSet = []
	for i in range(ballCount):
		generatedPoint = False
		while not generatedPoint:
			newCoord = ((areaDimensions[0]*-0.5)+random()*areaDimensions[0], (areaDimensions[1]*-0.5)+random()*areaDimensions[1])
			if inAcceptableRegion(newCoord, centreRadius) and notIntersecting(newCoord, coordSet, minimumBallDistance):
				coordSet.append(newCoord)
				generatedPoint = True
				
	#Read the xml for our model
	with open("greenSphere.sdf", "r") as f:
		ball_xml = f.read()
		
	#define an orientation
	orientation = Quaternion(0,0,0,0)

	#Now, spawn a ball at each of these coordinates
	for i in range(len(coordSet)):
		
		#Wait for service to respond, ensuring that requests don't get missed
		rospy.wait_for_service("gazebo/spawn_sdf_model")
		
		#Format a spawn request
		request = SpawnModelRequest()
		request.initial_pose.position.x=coordSet[i][0]
		request.initial_pose.position.y=coordSet[i][1]
		request.initial_pose.position.z=0.25
		request.model_xml = ball_xml
		request.model_name = "Ball" + str(i)
		
		#Try to spawn, printing a message if it fails
		if not spawnModelProxy(request):
			print "Unable to spawn model."

	print "Spawned models"	
